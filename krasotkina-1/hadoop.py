from mrjob.job import MRJob

class mrwordfrequencycount(MRJob):
    def mapper(self, _, line):
        if len(line.split()) == 0:
            yield "longest word", [("", 0)]
        else:
            mx_len = max(map(len, line.split()))
            yield "longest word", [elem for elem in
                (zip(line.split(), map(len, line.split()))) if elem[1] == mx_len]
    def reducer(self, key, values):
        values = [j for i in values for j in i]
        mx_len = max(map(lambda x: x[1], values))
        ans = [elem[0] for elem in values if elem[1] == mx_len]
        if len(ans) == 0:
            yield key, 0, [""]
        else:
            yield key, (len(ans[0]), ans)

if __name__ == '__main__':
    mrwordfrequencycount.run()
