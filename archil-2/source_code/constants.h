#ifndef CONSTANTS_H
#define CONSTANTS_H

#include "stdtypes.h"

/// ##########################################################
/// Тут описаны все константы из исходников Арчила Ивериевича
/// Я поменял Code_value_bits на 32, иначе таблицы частоты // а раньше было 30
/// просто не проходят проверку на кошерность.
/// На качество работы это не влияет, просто вам меньше забот
/// ##########################################################


/// ####################
/// ARI CONSTANTS ######
/// ####################
///

const uint No_of_chars = 256;               /* Number of characters               */
                                         /* Character indices are from 0 to No_of_chars-1 */
const uint No_of_symbols = No_of_chars + 1;   /* Total number of symbols            */
                                         /* Symbol indices are from 0 to No_of_symbols-1  */
const uint EOF_symbol = No_of_chars;        /* Index of the additional EOF symbol */

// SIZE OF ARITHMETIC CODE VALUES
const uint Code_value_bits = 32;                             /* Number of bits in a code value               */
const uint Max_cum_frequency = (1 << (Code_value_bits - 2)) - 1; /* Maximum allowed (cumulative) frequency count */


/// ##########################
/// ENCODERS CONSTANTS #######
/// ##########################

// SIZE OF ARITHMETIC CODE VALUES
// const uint Code_value_bits = 16;                     /* Number of bits in a code value   */
typedef uint Code_value;                              /* Type of an arithmetic code value */
// code_value contains at least Code_value_bits bits
typedef unsigned long Range_value;                    /* Type of size of a code region    */
// range_value contains at least (2*Code_value_bits-2) bits

// TOP POINT IN THE CODE VALUE RANGE
const Code_value Top_value = (1 << Code_value_bits) - 1; /* Largest code value (65535) */
// HALF AND QUARTER POINTS IN THE CODE VALUE RANGE
const Code_value First_qtr = Top_value / 4 + 1;    /* Point after first quarter (16384) */
const Code_value Half = 2 * First_qtr;           /* Point after first half    (32768) */
const Code_value Third_qtr = 3 * First_qtr;      /* Point after third quarter (49152) */

#endif // CONSTANTS_H
