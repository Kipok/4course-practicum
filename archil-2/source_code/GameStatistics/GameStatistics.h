#ifndef GAME_STATISTICS_HEADER
#define GAME_STATISTICS_HEADER

#include "Utility/Utility.h"

#include <vector>
#include <tuple>
#include <iostream>
#include <fstream>
#include <random>
#include <ctime>
#include <cstring>
#include <string>
#include <memory>

/// ###################################################################################################
/// Класс, отвечающий за игровую статистику
/// Он полностью отлажен и оттестирован (ну, я так думаю)
/// Он умеет:
///     1) Подсчитывать всю стату за одну игру
///     2) Итерационно агрегировать стату за несколько игр (то, что написано в требованиях к заданию)
///     3) Выводить все в csv файлик, который можно сразу прочитать пандасом
/// ###################################################################################################

class GameStatistics
{
    static const uint L = 8;
    ull tact_count;
    ull current_tact;

public:

    std::vector< double > reward;
    std::vector< double > profit;
    std::vector< double > optimality_ind;
    std::vector< double > compression_ratio;
    std::vector< double > space_save;

    GameStatistics (ull _tact_count) :
        tact_count(_tact_count),
        current_tact(0),
        reward(tact_count),
        profit(tact_count),
        optimality_ind(tact_count),
        compression_ratio(0),
        space_save(tact_count)
    {}

    ~GameStatistics () {}

    //! Записать результаты очередного такта
    //! Параметры:
    //!     l - reward (положительный)
    //!     is_optimal - было ли действие за этот такт оптимальным
    void update_statistics (uint l, bool is_optimal = true);

    //! Усреднить результаты за предыдущие игры с текущим
    //! Параметры
    //!     current_run - сколько игр прошло (с учетом добавленной)
    //!     cur_run_statistics - статистика за эту игру (полностью заполненная)
    void update_average_statistics (ull current_run, GameStatistics &cur_run_statistics);

    //! Сохранить все в табличку
    void save_to_csv (const char *filename, const char *ratio_filename);

    //! Хз зачем. Раньше эти члены были private.
    const std::vector< double > & get_reward () const            { return reward; }
    const std::vector< double > & get_profit () const            { return profit; }
    const std::vector< double > & get_optimality_ind () const    { return optimality_ind; }
    const std::vector< double > & get_compression_ratio () const { return compression_ratio; }
    const std::vector< double > & get_space_save () const        { return space_save; }
};

#endif
