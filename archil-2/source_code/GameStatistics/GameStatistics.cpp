#include "GameStatistics/GameStatistics.h"

void GameStatistics::update_statistics (uint l, bool is_optimal) {
    int cur_reward = int(l);
    if (current_tact >= tact_count) {
        std::cout << "WHAT ARE YOU DOING?! DO NOT UPDATE STATISTICS MORE TIMES THAN TACT COUNT!" << std::endl;
        return;
    }
    reward[current_tact] = -cur_reward;
    if (!current_tact) {
        profit[current_tact] = -cur_reward;
    } else {
        profit[current_tact] = profit[current_tact - 1] - cur_reward;
    }
    optimality_ind[current_tact] = is_optimal;

    space_save[current_tact] = (1.0 * L - cur_reward) / L;
    compression_ratio.resize(1);
    compression_ratio.back() = (1.0 * (current_tact + 1) * L) / (-profit[current_tact]);

    current_tact++;
}

void GameStatistics::update_average_statistics (ull current_run, GameStatistics &cur_run_statistics) {
    double cur_run = current_run;
    //Current tact counts from 0 to N - 1, so we use tact + 1
    for(size_t i = 0; i < tact_count; i++) {
        reward[i] = (cur_run / (cur_run + 1)) * reward[i] +
                    (1 / (cur_run + 1)) * cur_run_statistics.reward[i];

        profit[i] = (cur_run / (cur_run + 1)) * profit[i] +
                    (1 / (cur_run + 1)) * cur_run_statistics.profit[i];

        optimality_ind[i] = (cur_run / (cur_run + 1)) * optimality_ind[i] +
                            (1 / (cur_run + 1)) * cur_run_statistics.optimality_ind[i];

        //FIXED
        space_save[i] = (1.0 * L + reward[i]) / L;
    }
    compression_ratio.insert(compression_ratio.end(),
            cur_run_statistics.compression_ratio.begin(),
            cur_run_statistics.compression_ratio.end());
    current_tact = tact_count;
}

void GameStatistics::save_to_csv (const char *filename, const char *ratio_filename) {
    std::ofstream out(filename);

    //printf prints doubles with comma separated, but i needed dot-separated
    out << "Reward,Profit,Optimality indicator,Space saved" << std::endl;
    for(size_t i = 0; i < tact_count; i++) {
        out << reward[i] << "," <<
               profit[i] << "," <<
               optimality_ind[i] << "," <<
               space_save[i] << "\n";
    }
    out.close();

    std::ofstream ratio_out(ratio_filename);
    ratio_out << "Compression ratio" << std::endl;
    for (const auto &val : compression_ratio) {
        ratio_out << val << '\n';
    }
    ratio_out.close();
}
