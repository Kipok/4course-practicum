#ifndef ALLHEADERS_H
#define ALLHEADERS_H

#include <vector>
#include <iostream>
#include <fstream>
#include <random>
#include <array>
#include <chrono>

#include <ctime>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>

#include "Source/Source.h"
#include "SourceModel/SourceModel.h"
#include "Utility/Utility.h"
#include "Encoder/ARI_main.h"
#include "ReinforcementLearningGame/ReinforcementLearningGame.h"
#include "GameStatistics/GameStatistics.h"
#include "constants.h"
#include "stdtypes.h"

#endif // ALLHEADERS_H
