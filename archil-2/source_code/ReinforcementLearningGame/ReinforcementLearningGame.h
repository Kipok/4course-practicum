#ifndef RLGAME_HEADER
#define RLGAME_HEADER

#include "stdtypes.h"
#include "GameStatistics/GameStatistics.h"
#include "Utility/Utility.h"
#include "Encoder/ARI_main.h"

#include <vector>
#include <map>
#include <tuple>
#include <iostream>
#include <fstream>
#include <random>
#include <ctime>
#include <cstring>
#include <string>
#include <memory>
#include <cstdio>


/// ############################################################################
/// Класс, отвечающий за, собственно игру
/// У него два шаблонных параметра: класс-источник и класс-модель
/// Предполагается, что у обоих есть необходимые методы. Для максимальной простоты я сделал 4 базовых класса,
/// которые заставят вас реализовать все методы и по умолчанию умеющие проверять все данные на корректность.
/// За подробностями - в папки сорс и сорс модел, там подробно расписано, что вам делать.
///
/// Класс умеет:
///     а) кодировать-декодировать.
///     б) проводить игру и записывать всю усредненную стату в average_statistics
///     в) сохранять стату.
///
/// Ниже расписано, что какая функция делает



template <typename Source, typename SourceModel>
class ReinforcementLearningGame
{
    std::vector< Source > sources;
    std::vector< SourceModel > source_models;

    std::vector< ull > source_tact_count;
    std::vector< ull > source_model_tact_count;

    int source_ptr;
    int source_model_ptr;

    ull tact_count;
    uint games_played;

    GameStatistics average_statistics;

public:

    ReinforcementLearningGame (
            std::vector< Source > _sources,
            std::vector< SourceModel > _source_models,
            ull _tact_count) :
        sources(_sources),
        source_models(_source_models),
        tact_count(_tact_count),
        games_played(0),
        average_statistics(GameStatistics(tact_count))
    {
        source_tact_count.assign(sources.size(), tact_count / sources.size());
        for (size_t i = 0; i < tact_count % sources.size(); ++i) {
            source_tact_count[i]++;
        }
        source_tact_count.back()++;
        source_model_tact_count.assign(source_models.size(), tact_count / source_models.size());
        for (size_t i = 0; i < tact_count % source_models.size(); ++i) {
            source_model_tact_count[i]++;
        }
        source_model_tact_count.back()++;
        for (size_t i = 0; i < sources.size(); ++i) {
            sources[i].initialize(source_tact_count[i]);
        }
        for (size_t i = 0; i < source_models.size(); ++i) {
            source_models[i].initialize(source_model_tact_count[i]);
        }
    }

    ~ReinforcementLearningGame () {}

    //!
    //! \brief encode - функция кодировки. Сохраняет только закодированный файл
    //! \param encoded_stream - имя закодированного файла
    //! \return Игровую статистику за этот период
    //!
    GameStatistics encode (const char *encoded_stream);
    //!
    //! \brief encode - функция кодировки. Сохраняет и закодированный файл, и поток символов
    //! \param encoded_stream - имя закодированного файла
    //! \param symbol_stream - имя файла, куда записать исходный поток символов
    //! \return Игровую статистику за этот период
    //!
    GameStatistics encode (const char *encoded_stream, const char *symbol_stream);

    //!
    //! \brief decode - раскодирует файл. Для ускорения мы в процессе игры не раскодируем файлы
    //! \param encoded_stream - имя файла ,в котором закодированный поток
    //!
    void decode (const char *encoded_stream);
    //!
    //! \brief decode - раскодирует файл. Для ускорения мы в процессе игры не раскодируем файлы
    //! \param encoded_stream - имя файла ,в котором закодированный поток
    //! \param decoded_stream - имя файла, в который мы запишем раскодированный поток. Потом можете diff пройтись, если хотите
    //!
    void decode (const char *encoded_stream, const char *decoded_stream);

    //!
    //! \brief play_several_games - Играет несколько игр из разных нач. приближений.
    //! Код написан так, что гарантируется в случае статичных сред и моделей, что мы будем получать
    //! те же самые параметры на начало каждой игры (как и нужно)
    //! \param games_count - сколько раз играем
    //!
    void play_several_games (const uint games_count, bool check = false);

    //! Сами догадаетесь, что эта функция делает
    //! \brief save_average_statistics_csv
    //! \param filename
    //!
    void save_average_statistics_csv (const char *filename, const char *ratio_filename) {
        average_statistics.save_to_csv(filename, ratio_filename); 
    }


    GameStatistics &get_average_statistics () { return average_statistics; }

    void reset () {
        for (size_t i = 0; i < sources.size(); ++i) {
            sources[i].reset();
        }
        for (size_t i = 0; i < source_models.size(); ++i) {
            source_models[i].reset();
        }
        source_ptr = 0;
        source_model_ptr = 0;
        source_tact_count.assign(sources.size(), tact_count / sources.size());
        for (size_t i = 0; i < tact_count % sources.size(); ++i) {
            source_tact_count[i]++;
        }
        source_tact_count.back()++;
        source_model_tact_count.assign(source_models.size(), tact_count / source_models.size());
        for (size_t i = 0; i < tact_count % source_models.size(); ++i) {
            source_model_tact_count[i]++;
        }
        source_model_tact_count.back()++;
    }

    const Source &get_source () const {
        assert(source_ptr < int(sources.size()));
        return sources[source_ptr];
    }

    Source &get_source () {
        assert(source_ptr < int(sources.size()));
        return sources[source_ptr];
    }

    const SourceModel &get_source_model () const {
        assert(source_model_ptr < int(source_models.size()));
        return source_models[source_model_ptr];
    }

    SourceModel &get_source_model () {
        assert(source_model_ptr < int(source_models.size()));
        return source_models[source_model_ptr];
    }

    void process_tact () {
        if (!(--source_tact_count[source_ptr])) {
            ++source_ptr;
        }
        if (!(--source_model_tact_count[source_model_ptr])) {
            ++source_model_ptr;
        }
    }
};


template< typename Source, typename SourceModel >
void
ReinforcementLearningGame< Source, SourceModel >::play_several_games (
        const uint games_count, bool check) {
    const char *temp_encoded_filename = "temp.temp";
    const char *temp_symbol_filename = "symbol.temp";
    const char *temp_decoded_filename = "decoded.temp";

    GameStatistics results(tact_count);

    for (size_t i = 0; i < games_count; i++, games_played++) {
        std::cout << i;
        std::cout.flush();
        reset();
        if (!check) {
            results = encode(temp_encoded_filename);
        } else {
            results = encode(temp_encoded_filename, temp_symbol_filename);
            //Перед декодировкой обязательно все сбрасываем
            reset();
            decode(temp_encoded_filename, temp_decoded_filename);
            std::cout << ":" << check_coding(temp_symbol_filename, temp_decoded_filename);
        }
        std::cout << std::endl;
        average_statistics.update_average_statistics(games_played, results);
    }
    remove(temp_encoded_filename);
    remove(temp_symbol_filename);
    remove(temp_decoded_filename);
}

template< typename Source, typename SourceModel >
GameStatistics
ReinforcementLearningGame< Source, SourceModel >::encode (const char *encoded_stream) {
    FILE *enc = fopen(encoded_stream, "wb");
    OutputBitStream obs(enc);
    AriEncoding encoder(obs);

    GameStatistics current_game_stats(tact_count);
    uint current_symbol, l;
    bool is_optimal = true;

    for(size_t i = 0; i < tact_count; i++) {
        current_symbol = get_source().get_symbol();

        l = encoder.encode_symbol(current_symbol,
                get_source_model().get_cum_freq_table().get_table());
        is_optimal = get_source().is_optimal(current_symbol) && 
            get_source_model().is_optimal(current_symbol);

        get_source().update(current_symbol, l);
        get_source_model().update(current_symbol, l);

        current_game_stats.update_statistics(l, is_optimal);
        process_tact();
    }
    encoder.encode_symbol(EOF_symbol, get_source_model().get_cum_freq_table().get_table());
    encoder.done_encoding();

    fclose(enc);

    return current_game_stats;
}


template< typename Source, typename SourceModel >
GameStatistics
ReinforcementLearningGame< Source, SourceModel >::encode (
        const char *encoded_stream,
        const char *symbol_stream) {
    FILE *enc = NULL;
    enc = fopen(encoded_stream, "wb");
    std::ofstream symb(symbol_stream, std::ofstream::out);

    OutputBitStream obs(enc);
    AriEncoding encoder(obs);

    GameStatistics current_game_stats(tact_count);
    uint current_symbol, l;
    bool is_optimal = true;

    for(size_t i = 0; i < tact_count; i++) {
        current_symbol = get_source().get_symbol();
        symb << char(current_symbol);

        l = encoder.encode_symbol(current_symbol,
                get_source_model().get_cum_freq_table().get_table());
        is_optimal = get_source().is_optimal(current_symbol) &&
            get_source_model().is_optimal(current_symbol);

        get_source().update(current_symbol, l);
        get_source_model().update(current_symbol, l);

        current_game_stats.update_statistics(l, is_optimal);
        process_tact();
    }
    encoder.encode_symbol(EOF_symbol, get_source_model().get_cum_freq_table().get_table());
    encoder.done_encoding();

    fclose(enc);
    symb.close();

    return current_game_stats;
}

template< typename Source, typename SourceModel >
void
ReinforcementLearningGame< Source, SourceModel >::decode (const char *encoded_stream) {
    FILE *enc = fopen(encoded_stream, "rb");

    InputBitStream ibs(enc);
    AriDecoding decoder(ibs);

    uint current_symbol, l;

    for(size_t i = 0; i < tact_count; i++) {
        std::tie(current_symbol, l) =  decoder.decode_symbol(
                get_source_model().get_cum_freq_table().get_table());

        get_source_model().update(current_symbol, l);
        process_tact();
    }

    fclose(enc);
}

template< typename Source, typename SourceModel >
void
ReinforcementLearningGame< Source, SourceModel >::decode (
        const char *encoded_stream,
        const char *decoded_stream) {
    FILE *enc = fopen(encoded_stream, "rb");
    std::ofstream symb(decoded_stream, std::ofstream::out);

    InputBitStream ibs(enc);
    AriDecoding decoder(ibs);

    uint current_symbol, l;

    for(size_t i = 0; i < tact_count; i++) {
        std::tie(current_symbol, l) =  decoder.decode_symbol(
                get_source_model().get_cum_freq_table().get_table());
        symb << char(current_symbol);
        get_source_model().update(current_symbol, l);
        process_tact();
    }

    fclose(enc);
    symb.close();
}

#endif
