#ifndef __SOURCE_MODEL_HEADER
#define __SOURCE_MODEL_HEADER

#include "stdtypes.h"
#include "Utility/Utility.h"

#include <vector>
#include <iostream>
#include <fstream>
#include <random>
#include <ctime>
#include <cstring>
#include <algorithm>

//! Итак, вы добрались до файла с моделью. Вы здесь видите кучу классов, но не надо пугаться
//! По порядку:
//!     а) Класс ModelBase - определяет интерфейс модели. В нем прописаны чистые виртуальные
//!        функции, которые должны быть в любом модели
//!
//!     б) Класс ModelEnvironmentBase - является базовым для любой модели, которые выступает как среда.
//!        В нем уже переопределена большая часть функций, гарантирующая нам, что все будет работать корректно.
//!
//!     в) Класс ModelAgentBase - является базовым для любой модели-агента. И таки да, он пустой!
//!        Вам придется переопределить все функции самостоятельно.
//!
//! Ниже подробно расписано, что какая функция делает. Также есть примеры, как закодить модель-среду и модель-агента.
//!
//! Также важное замечание - класс ReinforcementLearningGame сделан так, что у нас одна функция игры на все случаи жизни.
//! Это значит, что независимо от того, адаптивные мы или нет, у любой среды и любого источника должна быть функция update.
//! Просто для неадаптивного она пустая - это зашито в базовом классе для ModelEnvironmentBase
//!
//! В целом, процесс создания своего источника таков
//!
//! 1) Для создания источника вам нужно отнаследоваться либо от класса ModelEnvironmentBase, либо от ModelAgentBase
//!    Если вы хотите стать статичной средой, то наследуете ModelEnvironmentBase, если агентом - ModelAgentBase
//! 2) Переопределяете все чистые виртуальные функции, которые не были переопределены


//!
//! СВЕРХВАЖНО:
//! Арифметический кодировщик работает таким образом, что при декодировке модель проходит ровно тот же путь (обучение),
//! что и при кодировке. Следовательно, для корректной декодировки любая (!) модель должна удовлетворять следующим условиям:
//!     1) Функция update должна быть детерминированной, никакой случайности в ней быть не должно
//!     2) Мы обязаны раз и навсегда зафиксировать начальное приближение для модели и использовать его каждый раз как при
//!        кодировке, так и при декодировке
//!



class ModelBase
{
public:
    static const size_t symbol_count = No_of_chars;

    //! Эта переменная нужна для кодировщика Арчила Ивериевича. Посколько именно ей он оперирует при кодировке
    //! символов, то если параметры модели обновляются, то сразу же необходимо обносить и эту переменную. пример ниже.
    CumFreqTable cum_freq;

    //!
    //! \brief initialize Начальная инициализация класса
    //! \param tact_count Истинное количество тактов
    //!
    //! Эта функция вызывается единственный раз при создании объекта класса ReinforcementLearningGame
    //! Передавать в конструктор класса параметр tact_count не нужно - класс RLGame передаст свой параметр
    //! Здесь делается вся начальная инициализация параметров, зависящих от tact_count,
    //! а также делается КОПИЯ ВСЕХ начальных данных. В функции reset мы должны сбрасывать каждый раз
    //! на одни и те же параметры, чтобы можно было корректно декодировать
    //!
    //! Эта функция вызывается единственный раз при создании объекта класса ReinforcementLearningGame
    virtual void initialize (const uint tact_count) = 0;

    //!
    //! \brief update Функция, отвечающая за обновление модели.
    //! \param symbol Текущий символ
    //! \param l      Награда за символ (положительная)
    //!
    //! Имеет смысл только для модели-агента - для модели среды она переопределена как пустая
    //!
    //! Вызывается на каждом такте каждой игры. Если вызов этой функции вдруг обновляет
    //! что-то в модели, то в конце вы ОБЯЗАТЕЛЬНО должны обновить кум.таблицу. Пример ниже
    virtual void update (const uint symbol, uint l) = 0;

    //!
    //! \brief reset Фукнция сброса всех параметров, отвечающих за поведение источника, на начальные данные перед началом новой игры
    //!
    //! Для источника-среды уже переопределена
    //! Для источника-агента: вы должны сбросить ВСЕ параметры на те значения, которые вы сохранили в функции initialize,
    //! чтобы на момент начала каждой новой игры у модели были одни и те же стартовые параметры.
    virtual void reset () = 0;

    virtual bool is_optimal (const uint symbol) = 0;

    //!
    //! \brief get_cum_freq_table Функция возвращает кумулятивную тамблицу символов
    //! \return
    //!
    //! Данная функция тут не зря - если вы накосячите, то при возврате кум.таблицы она сразу вам сообщит об этом.
    //! Кум. таблица нужна исключительно для кода Арчила Ивериевича. Вы можете вообще не знать, что это
    const CumFreqTable& get_cum_freq_table () {
        bool validity = check_cum_freq_validity();
        if (!validity) {
            fprintf(stderr, "OMG!! You made wrong cumulative frequency table! There may be two reasons for this:\n\
    1) Frequencies of some symbols are zero. They MUST be positive!\n\
    2) Maximum frequency has upper bound, so if cumulative frequency is greater than Max_cum_frequency =\
%d, you have to reduce frequencies or increase constant.\n", Max_cum_frequency);
            abort();
        }
        return cum_freq;
    }

    bool check_cum_freq_validity () { return cum_freq.is_valid(); }
};

class ModelEnvironmentBase: public ModelBase
{
public:

    void update (const uint symbol, uint l) {
        // Ничего не делаем, мы неадаптивная среда
    }

    // Сброс модели ничего не делает. От запуска к запуску она должна оставаться одной и той же.
    void reset () {}
};

class ModelAgentBase: public ModelBase
{
    //Тут нам тоже надо как-то зашить, что мы должны возвращаться к одному и тому же состоянию все время

public:
    // Мы адаптивная среда, оптимальность проверяет источник, нам не надо.
    bool is_optimal (const uint symbol) { return true; }
};

/// ###############################
/// EXAMPLE: MODEL ENVIRONMENT ####
/// ###############################


class UnadaptiveStatisticalModel: public ModelEnvironmentBase
{
    FreqTable frequency_table;
    uint max_freq_count;

public:

    //! Замечу, что во всех трех конструкторах в конце вызывается render_from_freq - это обязательно!

    UnadaptiveStatisticalModel (uint _max_freq_count = 10) :
        ModelEnvironmentBase(),
        max_freq_count(_max_freq_count)
    {
        for (size_t i = 0; i < symbol_count; i++) {
            frequency_table[i] = rand() % max_freq_count + 1;
        }
        cum_freq.render_from_freq(frequency_table);
    }

    UnadaptiveStatisticalModel (std::vector< uint > frequencies) :
        ModelEnvironmentBase(),
        max_freq_count(0)
    {
        for (size_t i = 0; i < symbol_count; i++) {
            frequency_table[i] = max(frequencies[i], (uint)(1));
        }
        cum_freq.render_from_freq(frequency_table);
    }

    UnadaptiveStatisticalModel (double alpha, uint tact) :
        ModelEnvironmentBase()
    {
        std::gamma_distribution< double > distribution(alpha, 1);
        std::default_random_engine generator(time(0));

        for (size_t j = 0; j < symbol_count; j++) {
            double x = distribution(generator) * double(tact) - 1;
            frequency_table[j] = max(x, 1.0);
        }
        cum_freq.render_from_freq(frequency_table);
    }

    const FreqTable& get_freq_table () const {
        return frequency_table;
    }

    bool is_optimal (const uint symbol) {
        auto check_val = frequency_table[symbol];
        for (size_t i = 0; i < symbol_count; ++i) {
            if (frequency_table[i] > check_val) {
                return false;
            }
        }
        return true;
    }

    virtual void initialize (const uint tact_count) {}

    void print_freq_table () {
        std::cout << "Freq table" << std::endl;
        uint sum = 0;
        for (size_t i = 0; i < symbol_count; i++) {
            sum += frequency_table[i];
        }
        for (size_t i = 0; i < symbol_count; i++) {
            std::cout << frequency_table[i] * 1.0 / sum << " ";
        }
        std::cout << std::endl;
    }
};

/// ###############################
/// EXAMPLE: MODEL AGENT ##########
/// ###############################

class AdaptiveOrder0StatisticalModelAgent: public ModelAgentBase
{
    FreqTable frequency_table;
    FreqTable initial_frequency_table;
    uint max_freq_count;

public:

    AdaptiveOrder0StatisticalModelAgent (uint _max_freq_count = 10) :
        ModelAgentBase(),
        max_freq_count(_max_freq_count)
    {
    }

    const FreqTable& get_freq_table () const {
        return frequency_table;
    }

    //! Здесь мы один раз и навсегда инициализируем initial_frequency_table
    //! Я не стал его делать const по одной простой причине:
    virtual void initialize (const uint tact_count) {
        for (size_t i = 0; i < symbol_count; i++) {
            frequency_table[i] = rand() % max_freq_count + 1;
        }
        initial_frequency_table = frequency_table;
        cum_freq.render_from_freq(frequency_table);
    }

    //! В конце ЛЮБОЙ функции update для источника-агента должен быть вызов последней строчки
    //! cum_freq.render_from_freq(frequency_table);

    void update (const uint symbol, uint l) {
        frequency_table[symbol]++;
        cum_freq.render_from_freq(frequency_table);
    }

    //! Сбрасываем все параметры на начальные
    void reset () {
        frequency_table = initial_frequency_table;
        cum_freq.render_from_freq(frequency_table);
    }

    void print_freq_table () {
        for (size_t i = 0; i < symbol_count; i++) {
            std::cout << frequency_table[i] << std::endl;
        }
    }
};

#endif // __SOURCE_MODEL_HEADER
