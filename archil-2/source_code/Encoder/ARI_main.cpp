// Suppress Microsoft nonstandard ideas
//#define _CRT_SECURE_NO_WARNINGS

#include "Encoder/ARI_main.h"

// Returns number of bits used to encode symbol
uint
AriEncoding::encode_symbol (const uint symbol, const uint* const cum_freq) {
    uint bits_written = obs.get_bit_counter();

    Range_value range = Range_value(high - low) + 1; /* Size of the current code region */
    high = low +                                     /* Narrow the code region   */
        (range * cum_freq[symbol]) / cum_freq[0] - 1;/* to that allotted to this */
    low = low +                                      /* symbol.                  */
        (range * cum_freq[symbol + 1]) / cum_freq[0];

    for (;;) {                                  /* Loop to output bits.     */
        if (high < Half) {
            bit_plus_follow(0);                 /* Output 0 if in low half. */
        } else if (low >= Half) {               /* Output 1 if in high half.*/
            bit_plus_follow(1);
            low -= Half;
            high -= Half;                       /* Subtract offset to top.  */
        } else if (low >= First_qtr && high < Third_qtr) {
                                                /* Output an opposite bit   */    
                                                /* later if in middle half. */
                ++bits_to_follow;
                low -= First_qtr;               /* Subtract offset to middle*/
                high -= First_qtr;
        } else {
            break;                              /* Otherwise exit loop.     */
        }
        low *= 2;
        high = 2 * high + 1;                    /* Scale up code range.     */
    }

    return obs.get_bit_counter() - bits_written;
}

// DECODE NEXT SYMBOL
std::tuple< uint, uint >
AriDecoding::decode_symbol (const uint* const cum_freq) {
    uint bits_written = ibs.get_bit_counter();

    Range_value range = Range_value(high - low) + 1;        /* Size of current code region    */
    Code_value cum = Code_value                             /* Cumulative frequency for value */
        (((Range_value(value - low) + 1) * cum_freq[0] - 1) / range);
    uint symbol;                                            /* Symbol decoded      */
    for (symbol=1; cum_freq[symbol] > cum; ++symbol);       /* Find symbol for cum */
    --symbol;

    high = low +                                            /* Narrow the code region   */
        (range * cum_freq[symbol]) / cum_freq[0] - 1;       /* to that allotted to this */
    low = low +                                             /* symbol.                  */
        (range * cum_freq[symbol + 1]) / cum_freq[0];
    for (;;) {                                              /* Loop to get rid of bits. */
        if (high < Half) {
            /* nothing */                                   /* Expand low half.         */
        } else if (low >= Half) {                           /* Expand high half.        */
            value -= Half;
            low -= Half;                                    /* Subtract offset to top.  */
            high -= Half;
        } else if (low >= First_qtr && high < Third_qtr) {  /* Expand middle half.      */
                value -= First_qtr;
                low -= First_qtr;                           /* Subtract offset to middle*/
                high -= First_qtr;
        } else {
            break;                                          /* Otherwise exit loop.     */
        }
        low *= 2;
        high = 2 * high + 1;                                /* Scale up code range.     */
        value = 2 * value + ibs.input_bit();                    /* Move in next input bit.  */
    }
    return std::tuple< uint, uint >(symbol, bits_written - ibs.get_bit_counter());
}
