#ifndef __ARICODING_H
#define __ARICODING_H

#include "stdtypes.h"
#include "Utility/Utility.h"
#include "SourceModel/SourceModel.h"

#include <vector>
#include <tuple>
#include <iostream>
#include <fstream>
#include <ctime>
#include <cstring>
#include <string>
#include <memory>

/// ##################################################################
/// В этом файле описан кодировщик Арчила Ивериевича
/// Вам здесь делать нечего
/// ##################################################################

class AriEncoding
{
    // CURRENT STATE OF THE ENCODING
    Code_value low, high;           /* Ends of the current code region                      */
    unsigned long bits_to_follow;   /* Number of opposite bits to output after the next bit */

    OutputBitStream &obs;

    // OUTPUT THE BIT PLUS FOLLOWING OPPOSITE BITS, takes 0 or 1
    void bit_plus_follow (const int bit) {
        obs.output_bit(bit);                         /* Output the bit.          */
        while (bits_to_follow) {
            obs.output_bit(!bit);                    /* Output bits_to_follow    */
            --bits_to_follow;                        /* opposite bits.           */
        }
        /*Now bits_to_follow is zero. */
    }

public:

    // START ENCODING A STREAM OF SYMBOLS
    void start_encoding () {
        low = 0;                                    /* Full code range.         */
        high = Top_value;
        bits_to_follow = 0;                         /* No bits to follow next.  */
    }

    // FINISH ENCODING THE STREAM
    void done_encoding () {
        ++bits_to_follow;                           /* Output two bits that     */
        if (low < First_qtr) {
            bit_plus_follow(0);                     /* select the quarter that  */
        } else {
            bit_plus_follow(1);                     /* the current code range   */
        }
        obs.done_outputing_bits();
    }

    AriEncoding(OutputBitStream &_obs) : obs(_obs) {
        start_encoding();
    }
    ~AriEncoding() {}

    uint encode_symbol (const uint symbol, const uint* const cum_freq);
};

class AriDecoding
{
    // CURRENT STATE OF THE DECODING
    Code_value value;        /* Currently-seen code value   */
    Code_value low, high;    /* Ends of current code region */

    InputBitStream &ibs;

public:
    // CONSTRUCTOR
    AriDecoding (InputBitStream &_ibs) : ibs(_ibs) {
        low = 0;                                        /* Full code range.         */
        high = Top_value;
        value = 0;                                      /* Input bits to fill the   */
        for (uint i = 0; i < Code_value_bits; ++i) {    /* code value.              */
            value = 2 * value + ibs.input_bit();
        }
    }

    // DECODE NEXT SYMBOL
    std::tuple< uint, uint > decode_symbol (const uint* const cum_freq);
};

#endif
