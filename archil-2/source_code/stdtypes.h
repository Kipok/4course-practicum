#ifndef STDTYPES_H
#define STDTYPES_H

#include <cstdint>
#include <cinttypes>

typedef uint32_t uint;
typedef uint64_t ull;

template< typename T > T max(T x, T y) { return (x > y) ? x : y; }

#endif // STDTYPES_H
