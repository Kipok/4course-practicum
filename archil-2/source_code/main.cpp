#include "AllHeaders.h"

int main(int argc, char *argv[]) {
    uint tact_num = 524288;
    uint games_num = 100;

    ExpertSource src;
    UnadaptiveStatisticalModel mod1(0.01, 1000);
    UnadaptiveStatisticalModel mod2(0.1, 3000);
    std::vector< ExpertSource > sources = {src};
    std::vector< UnadaptiveStatisticalModel > source_models;
    source_models.push_back(mod1);
    source_models.push_back(mod2);
    ReinforcementLearningGame< ExpertSource, UnadaptiveStatisticalModel >
        gamer1(sources, source_models, tact_num);
    gamer1.play_several_games(games_num);
    gamer1.save_average_statistics_csv("game_12.csv", "comp_12.csv");

    sources[0].reset();
    source_models.clear();
    source_models.push_back(mod2);
    source_models.push_back(mod1);
    ReinforcementLearningGame< ExpertSource, UnadaptiveStatisticalModel >
        gamer2(sources, source_models, tact_num);
    gamer2.play_several_games(games_num);
    gamer2.save_average_statistics_csv("game_21.csv", "comp_21.csv");

    sources[0].reset();
    source_models.clear();
    source_models.push_back(mod1);
    ReinforcementLearningGame< ExpertSource, UnadaptiveStatisticalModel >
        gamer3(sources, source_models, tact_num);
    gamer3.play_several_games(games_num);
    gamer3.save_average_statistics_csv("game_1.csv", "comp_1.csv");

    sources[0].reset();
    source_models.clear();
    source_models.push_back(mod2);
    ReinforcementLearningGame< ExpertSource, UnadaptiveStatisticalModel >
        gamer4(sources, source_models, tact_num);
    gamer4.play_several_games(games_num);
    gamer4.save_average_statistics_csv("game_2.csv", "comp_2.csv");
}
