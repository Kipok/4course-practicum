#include "Utility/Utility.h"

std::vector< uint >
convert_probabilities_to_frequencies (
        std::vector< double > &probabilities,
        uint tact_count) {
    std::vector< uint > res(No_of_chars);
    for(size_t i = 0; i < No_of_chars; i++) {
        int x = probabilities[i] * tact_count - 1;
        res[i] = x > 1 ? x : 1;
    }
    return res;
}

bool check_coding (const char *initial_stream, const char *decoded_stream) {
    FILE *f = fopen(initial_stream, "rb");
    FILE *g = fopen(decoded_stream, "rb");

    //Проверим, что файлики одинаковой длины
    fseek(f, 0L, SEEK_END);
    size_t f_size = ftell(f);
    fseek(f, 0L, SEEK_SET);

    fseek(g, 0L, SEEK_END);
    size_t g_size = ftell(g);
    fseek(g, 0L, SEEK_SET);

    if (f_size != g_size) {
        fprintf(stderr, "Input stream and decoded stream lenghts are different\n");
        fclose(f);
        fclose(g);
        return false;
    }


    uint c1, c2;
    while (((c1 = getc(f)) != EOF) && ((c2 = getc(g)) != EOF)) {
        if (c1 != c2) {
            fprintf(stderr, "Input stream and decoded stream symbols are different\n");
            fclose(f);
            fclose(g);
            return false;
        }
    }

    fclose(f);
    fclose(g);
    return true;
}

// INITIALIZE THE TABLE
// The actual initialization should be done in a source model.
void FreqTable::init_table () {
    // uniform distribution
    for (uint i = 0; i < No_of_symbols; ++i) {    /* Set up initial frequency counts */
        freq[i] = 1;                              /* to be one for all the symbols.  */
    }
}

// CONSTRUCTOR
FreqTable::FreqTable () {
    init_table();
}

uint FreqTable::operator[] (const uint symbol) const
{
    assert(symbol < No_of_symbols);
    return freq[symbol];
}

uint& FreqTable::operator[] (const uint symbol)
{
    assert(symbol < No_of_symbols);
    return freq[symbol];
}

// HALVE ALL THE FREQUENCIES
// A valid table remains valid
void FreqTable::halve_table () {
    for (uint i = 0; i < No_of_symbols; ++i) {      /* Halve all the counts   */
        freq[i] = (freq[i] + 1) / 2;                /* keeping them non-zero  */
    }
}

// RENDER FREQUENCY TABLE FROM CUMULATIVE FREQUENCY TABLE
void FreqTable::render_from_cum_freq (const CumFreqTable &cum_freq) {
    for (uint i = 0; i < No_of_symbols; ++i) {
        freq[i] = cum_freq[i] - cum_freq[i + 1];
    }
}

// GET THE SUM OF ALL THE FREQUENCIES
uint FreqTable::sum () const {
    return std::accumulate(std::begin(freq), std::begin(freq) + No_of_symbols, (uint)(0));
}

// CHECK WHETHER THE FREQUENCY VALUES ARE CORRECT
bool FreqTable::is_valid () const {
    uint sum = 0;
    for (uint i = 0; i < No_of_symbols; ++i) {
        if (!freq[i]) {
            return false;
        }
        sum += freq[i];
    }
    return sum <= Max_cum_frequency;
}

// CUMULATIVE FREQUENCY TABLE
// class CumFreqTable

// INITIALIZE THE TABLE
// The actual initialization should be done in a source model.
void CumFreqTable::init_table () {
    // uniform distribution
    for (uint i = 0; i <= No_of_symbols; ++i) {     /* Set up initial frequency          */
        cum_freq[i] = No_of_symbols - i;            /* counts to be one for all symbols. */
    }
}

// CONSTRUCTOR
CumFreqTable::CumFreqTable () {
    init_table();
}

uint CumFreqTable::operator[] (const uint symbol) const {
    assert(symbol <= No_of_symbols);
    return cum_freq[symbol];
}

uint& CumFreqTable::operator[] (const uint symbol) {
    assert(symbol <= No_of_symbols);
    return cum_freq[symbol];
}

// RENDER CUMULATIVE FREQUENCY TABLE FROM FREQUENCY TABLE
void CumFreqTable::render_from_freq (const FreqTable &freq) {
    uint cum = 0;
    for (uint i = No_of_symbols; i > 0; ) {
        cum_freq[i] = cum;
        cum += freq[--i];
    }
    cum_freq[0] = cum;
}

// CHECK WHETHER THE FREQUENCY VALUES ARE CORRECT
bool CumFreqTable::is_valid () const {
    if (cum_freq[0] > Max_cum_frequency) {
        return false;
    }
    for (uint i = 0; i < No_of_symbols; ++i) {
        if (cum_freq[i] <= cum_freq[i + 1]) {
            return false;
        }
    }
    if (cum_freq[No_of_symbols]) {
        return false;
    }
    return true;
}
