#ifndef __UTILITY_HEADER
#define __UTILITY_HEADER

#include "constants.h"
#include "stdtypes.h"

#include <algorithm>
#include <vector>
#include <iostream>
#include <fstream>

#include <ctime>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>

std::vector< uint >
convert_probabilities_to_frequencies(std::vector< double > &probabilities, unsigned int tact_count);

bool check_coding(const char *initial_stream, const char *decoded_stream);

/// ######################
/// BIT STREAMS ##########
/// ######################


// BIT INPUT ROUTINES
class InputBitStream
{
    // FILE STREAM FOR INPUT
    FILE* const in;

    // THE BIT BUFFER
    int buffer;                 /* Bits waiting to be input, must be int for getc() */
    uint bits_to_go;            /* Number of bits still in the buffer    */
    uint garbage_bits;          /* Number of bits past the end of file   */
                                /* to detect too many extra bits after EOF.  */

    // INITIALIZE BIT INPUT
    void start_inputing_bits () {
        buffer = 0;
        bits_to_go = 0;            /* Buffer starts out with no bits in it. */
        garbage_bits = 0;          /* No extra bits at the beginning        */
    }

    // THE BIT COUNTER
    uint bits_read;

public:
    // CONSTRUCTOR
    InputBitStream (FILE* f) : in(f), bits_read(0) {
        start_inputing_bits();
    }

    // DESTRUCTOR
    ~InputBitStream() {} //nothing to do

    // INPUT A BIT
    // returns 0 or 1, always returns 1 after EOF
    int input_bit () {
        ++bits_read;
        if (garbage_bits > 0) {
            ++garbage_bits;                             /* Return arbitrary bits*/
            if (garbage_bits > Code_value_bits - 2) {   /* after EOF, but check */
                fprintf(stderr,"Invalid input binary file: not enough bits\n");
                //exit(EXIT_FAILURE);                   /* for too many such.   */
            }
            return 1;
        }

        //TODO: refactor getc to some func, getting
        if (bits_to_go == 0) {                     /* Read next byte if no bits */
            buffer = getc(in);                     /* are left in the buffer.   */
            if (buffer == EOF) {
                ++garbage_bits;
                return 1;
            }
            bits_to_go = 8;
        }

        int t = buffer & 1;                        /* Return the next bit from */
        buffer >>= 1;                              /* the bottom of the byte.  */
        --bits_to_go;
        return t;
    }

    // GET BIT COUNTER
    uint get_bit_counter () const {
        return bits_read;
    }

    // RESET BIT COUNTER
    void reset_bit_counter () {
        bits_read = 0;
    }
};



// BIT OUTPUT ROUTINES
class OutputBitStream
{
    // FILE STREAM FOR OUTPUT
    FILE* const out;

    // THE BIT BUFFER.
    int buffer;                     /* Bits buffered for output       */
    uint bits_to_go;                /* Number of bits free in buffer  */

    // THE BIT COUNTER
    uint bits_written;

public:
    // CONSTRUCTOR
    OutputBitStream(FILE* f) : out(f), bits_written(0) {
        start_outputing_bits();
    }

    // DESTRUCTOR
    ~OutputBitStream()
    {
        //done_outputing_bits();
    }


    // INITIALIZE FOR BIT OUTPUT
    void start_outputing_bits () {
        buffer = 0;                 /* Buffer is empty to start with. */
        bits_to_go = 8;
    }

    // FLUSH OUT THE LAST BITS
    void done_outputing_bits () {
        putc((char) (buffer >> bits_to_go), out);
    }

    // OUTPUT A BIT, takes 0 or 1
    void output_bit (const int bit) {
        ++bits_written;
        buffer >>= 1;
        if (bit) {
            buffer |= 0x80;              /* Put bit on the top of the buffer. */
        }
        --bits_to_go;
        if (!bits_to_go) {              /* Output the buffer if it is full.  */
            putc((char)buffer, out);
            bits_to_go = 8;
        }
    }

    // GET BIT COUNTER
    unsigned int get_bit_counter () const {
        return bits_written;
    }

    // RESET BIT COUNTER
    void reset_bit_counter () {
        bits_written = 0;
    }
};




/// ######################
/// FREQUENCY TABLES #####
/// ######################


// forward declaration
class CumFreqTable;

// FREQUENCY TABLE
class FreqTable
{
private:
    // TABLE FOR SYMBOLS (not characters)
    uint freq[No_of_symbols];             /* Frequencies */

    // TABLE ASSUMPTIONS
    // 1 <= freq[0],..., 1 <= freq[No_of_chars]
    // freq[0]+...+freq[No_of_chars] <= Max_cum_frequency

private:
    // INITIALIZE THE TABLE
    // The actual initialization should be done in a source model.
    void init_table ();

public:
    // CONSTRUCTOR
    FreqTable ();

    // DESTRUCTOR
    // ~FreqTable () // nothing to do

    uint operator[] (const uint symbol) const;
    uint& operator[] (const uint symbol);

    // HALVE ALL THE FREQUENCIES
    // A valid table remains valid
    void halve_table ();

    // RENDER FREQUENCY TABLE FROM CUMULATIVE FREQUENCY TABLE
    void render_from_cum_freq (const CumFreqTable &cum_freq);

    // CHECK WHETHER THE FREQUENCY VALUES ARE CORRECT
    bool is_valid () const;

    // GET THE SUM OF ALL THE FREQUENCIES
    uint sum () const;

    const uint* get_table () const { return freq; }
};

// CUMULATIVE FREQUENCY TABLE
class CumFreqTable
{
private:
    // TABLE FOR SYMBOLS (not characters)
    uint cum_freq[No_of_symbols + 1];            /* Cumulative symbol frequencies    */

    // TABLE ASSUMPTIONS
    // cum_freq[No_of_symbols]=0, const
    // cum_freq[No_of_symbols-1]=freq[No_of_chars]
    // cum_freq[No_of_symbols-2]=freq[No_of_chars]+freq[No_of_chars-1]
    // ...
    // cum_freq[0]=freq[No_of_chars]+...+freq[0] <= Max_cum_frequency

private:
    // INITIALIZE THE TABLE
    // The actual initialization should be done in a source model.
    void init_table ();

public:
    // CONSTRUCTOR
    CumFreqTable ();

    // DESTRUCTOR
    // ~CumFreqTable () // nothing to do

    uint operator[] (const uint symbol) const;
    uint& operator[] (const uint symbol);

    // RENDER CUMULATIVE FREQUENCY TABLE FROM FREQUENCY TABLE
    void render_from_freq (const FreqTable &freq);

    // CHECK WHETHER THE FREQUENCY VALUES ARE CORRECT
    bool is_valid () const;

    const uint* get_table () const { return cum_freq; }
};

#endif //__UTILITY_HEADER
