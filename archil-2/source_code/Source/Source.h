#ifndef __SOURCE_HEADER
#define __SOURCE_HEADER

#include "stdtypes.h"
#include "Utility/Utility.h"

#include <vector>
#include <queue>
#include <iostream>
#include <fstream>
#include <random>
#include <ctime>
#include <cstring>
#include <cstdio>
#include <algorithm>

//! Итак, вы добрались до файла с источниками. Вы здесь видите кучу классов, но не надо пугаться
//! По порядку:
//!     а) Класс SourceBase - определяет интерфейс источника. В нем прописаны чистые виртуальные
//!        функции, которые должны быть в любом источнике
//!
//!     б) Класс SourceEnvironmentBase - является базовым для любого источника, которые выступает как среда.
//!        В нем уже переопределена большая часть функций, гарантирующая нам, что все будет работать корректно.
//!        Вам останется допилить только одну функцию
//!             generate_symbols_stream() - мы генерим сразу весь поток символов и сохраняем его, чтобы дальше
//!             раз за разом мы игрались с тем же набором символов и корректно собирали статистику
//!
//!     в) Класс SourceAgentBase - является базовым для любого источника-агента. И таки да, он пустой!
//!        Вам придется переопределить все функции самостоятельно.
//!
//! Ниже подробно расписано, что какая функция делает. Также есть примеры, как закодить источник-среду и источника-агента.
//!
//! Также важное замечание - класс ReinforcementLearningGame сделан так, что у нас одна функция игры на все случаи жизни.
//! Это значит, что независимо от того, адаптивные мы или нет, у любой среды и любого источника должна быть функция update.
//! Просто для неадаптивного она пустая - это зашито в базовом классе для SourceEnvironmentBase
//!
//! В целом, процесс создания своего источника таков
//!
//! 1) Для создания источника вам нужно отнаследоваться либо от класса SourceEnvironmentBase, либо от SourceAgentbase
//!    Если вы хотите стать статичной средой, то наследуете SourceEnvironmentBase, если агентом - SourceAgentbase
//! 2) Переопределяете все чистые виртуальные функции, которые не были переопределены


//!
//! \brief The SourceBase class
//! Тут я подробно распишу, что какая функция делает
//!
class SourceBase
{
public:
    static const size_t symbol_count = No_of_chars;
    //!
    //! \brief cur_index
    //! Переменная, отвечающая за номер текущего такта игры
    ull cur_index;

    //!
    //! \brief tact_count
    //! Переменная, отвечающая за общее число тактов в игре
    ull tact_count;

    //! Инициализируем их нулем. Внимание!
    //! Внимание!
    //! истинное значение переменной tact_count придет в функции initialize!
    SourceBase (): cur_index(0), tact_count(0) {}
    virtual ~SourceBase () {}

    //!
    //! \brief update Функция, отвечающая за обновление модели.
    //! \param symbol Текущий символ
    //! \param l      Награда за символ (положительная)
    //!
    //! Имеет смысл только для источника-агента - для источника среды она переопределена как пустая
    //!
    //! Вызывается на каждом такте каждой игры
    virtual void update (const uint symbol, uint l) = 0;

    //!
    //! \brief get_symbol Берет очередной символ из потока
    //! \return собсна, символ
    //!
    //! Для источника-среды уже переопределена - ее необходимо сделать для агента.
    virtual uint get_symbol () = 0;

    //!
    //! \brief reset Фукнция сброса всех параметров, отвечающих за поведение источника, на начальные данные перед началом новой игры
    //!
    //! Для источника-среды уже переопределена
    //! Для источника-агента: вы должны сбросить ВСЕ параметры на те значения, которые должны быть в начале любой игры
    //! Эта функция вызывается перед началом КАЖДОЙ игры.
    virtual void reset () = 0;

    //!
    //! \brief initialize Начальная инициализация класса
    //! \param tact_count Истинное количество тактов
    //!
    //! Вот тут тонкий момент - на первый взгляд, непонятно, зачем она, если у нас есть reset.
    //! отвечаю: во избежание того, чтобы вы передали разное количество тактов в класс RL и в классы источников и сред,
    //! класс RL передает свое значение таков в эту функцию в конструкторе.
    //!
    //! Главное, что вы должны сделать в этой функции - выделить памяти на все свои переменные ровно столько, сколько требует
    //! tact_count.
    //!
    //! Эта функция вызывается единственный раз при создании объекта класса ReinforcementLearningGame
    virtual void initialize (const uint tact_count) = 0;

    virtual bool is_optimal (const uint symbol) = 0;
};

class SourceEnvironmentBase: public SourceBase
{
public:

    std::vector< uint > symbols_stream;

    SourceEnvironmentBase () : SourceBase(), symbols_stream(0) {}

    //! Ничего не делаем: мы статичная среда
    void update (const uint symbol, uint l) {}

    //! Поскольку мы статичная среда, то всегда берем символы из уже сохраненной последовательности
    uint get_symbol () {
        if (cur_index < tact_count) {
            return symbols_stream[cur_index++];
        } else {
            return EOF_symbol;
        }
    }

    //! Теперь мы каждый раз будем генерить новую последовательность
    void reset () {
        cur_index = 0;
        generate_symbols_stream();
    }

    //! Выделим память под поток и сгенерим его будущей переопределенной функцией.
    //! Гарантируется, что если вы запилите ее, то поток всегда будет сгенерирован ровно один раз
    //! и будет использоваться во всех дальнейших играх
    void initialize (uint _tact_count) {
        cur_index = 0;
        tact_count = _tact_count;
        symbols_stream.assign(tact_count, (uint)(0));
        generate_symbols_stream();
    }


    //! Отладочная штука, но пусть валяется тут
    void save_symbols_stream (FILE *filename) {
        for (size_t i = 0; i < tact_count; i++) {
            putc(symbols_stream[i], filename);
        }
    }

    //! Вот ее вам надо будет переопределить!
    //! У вас на момент вызова уже есть память под данные и количество данных, осталось лишь их заполнить
    virtual void generate_symbols_stream() = 0;

};


//!Да, он пустой! Все надо будет ручками переопределить
class SourceAgentBase: public SourceBase
{
public:
    bool is_optimal (const uint symbol) { return true; }
};


/// #################################
/// EXAMPLE: SOURCE ENVIRONMENT #####
/// #################################


//!
//! \brief The UnadaptiveOrder0Source class
//! Источник-среда 0 порядка. Генерим некоторые вероятности символов при создании класса
//! При инициализации генерим сразу поток символов.
//! Ну и усе!
//!
//!
class UnadaptiveOrder0Source: public SourceEnvironmentBase
{
    std::discrete_distribution< uint > symbol_probabilities;
    std::default_random_engine generator;

public:

    virtual ~UnadaptiveOrder0Source () {}
    UnadaptiveOrder0Source (double alpha = 1.0) :
        SourceEnvironmentBase(),
        generator(time(0))
    {
        std::gamma_distribution< double > distribution(alpha, 1.0);
        std::vector< double > v(symbol_count);

        for (auto &x : v) {
            x = distribution(generator);
        }

        symbol_probabilities = std::discrete_distribution< uint >(v.begin(), v.end());
    }

    void generate_symbols_stream () {
        for(ull i = 0; i < tact_count; i++) {
            symbols_stream[i] = symbol_probabilities(generator);
        }
    }

    std::vector <double> get_symbol_probabilities () const {
        return symbol_probabilities.probabilities();
    }

    std::vector< uint > get_symbol_frequencies () const {
        std::vector< uint > symbol_frequencies(symbol_count);
        for(size_t i = 0; i < tact_count; i++) {
            symbol_frequencies[symbols_stream[i]]++;
        }
        return symbol_frequencies;
    }

    bool is_optimal (const uint symbol) { return true; }
};

//!
//! \brief The UnadaptiveOrder1SymbolGenerator class
//! То же самое, только модель первого порядка
//! Это значит, что мы генерим символы не из p(s),
//! а из p(s | s_prev).
//!
//! Более сложная среда, к которой идеально может подстроиться только агент 1 порядка
//!
class UnadaptiveOrder1SymbolGenerator: public SourceEnvironmentBase
{
    std::vector< std::discrete_distribution< uint > > transition_matrix;
    std::default_random_engine generator;
    uint prevous_symbol;

public:

    ~UnadaptiveOrder1SymbolGenerator () {}

    UnadaptiveOrder1SymbolGenerator (double alpha = 1.0) :
        SourceEnvironmentBase(),
        generator(time(0))
    {
        std::gamma_distribution< double > distribution(alpha, 1.0);
        std::vector< double > v(symbol_count);

        for(size_t i = 0; i < symbol_count; i++) {
            for (auto &x : v) {
                x = distribution(generator);
            }
            transition_matrix[i] = std::discrete_distribution< unsigned int > (v.begin(), v.end());
        }

        prevous_symbol = rand() % symbol_count;
    }


    void generate_symbols_stream () {
        symbols_stream[0] = prevous_symbol;

        for(ull i = 0; i < tact_count; i++) {
            uint cur_symbol = transition_matrix[prevous_symbol](generator);
            symbols_stream[i] = cur_symbol;
            prevous_symbol = cur_symbol;
        }
    }

    std::vector< std::vector< double > > get_transition_matrix_probabilities () const {
        std::vector< std::vector< double > > transition_matrix_probabilities(symbol_count);
        for(size_t i = 0; i < symbol_count; i++) {
            transition_matrix_probabilities[i] = transition_matrix[i].probabilities();
        }
        return transition_matrix_probabilities;
    }
};

/// #################################
/// EXAMPLE: SOURCE AGENT ###########
/// #################################

//!
//! \brief The AdaptiveOrder0Source class
//!
//! Наверно, самый сложный примерчик. В нем, по сути, сделан многорукий бандит
//! с soft-max стратегией исследования/эксплуатации.
class AdaptiveOrder0Source : public SourceAgentBase
{
    std::vector< double > symbol_rewards;
    std::vector< double > initial_symbol_rewards;
    std::vector< double > symbol_probabilities;
    std::vector< uint > symbol_frequencies;
    std::default_random_engine generator;
    uint max_col;

    double T, initial_T;

public:

    ~AdaptiveOrder0Source () {}

    AdaptiveOrder0Source (
            uint _max_col,
            bool _check_change = false,
            double _temperature = 2000.) :
        SourceAgentBase(),
        symbol_rewards(symbol_count),
        initial_symbol_rewards(symbol_count),
        symbol_probabilities(symbol_count),
        symbol_frequencies(symbol_count),
        generator(time(0)),
        max_col(_max_col),
        T(_temperature),
        initial_T(_temperature)
    {
    }

    virtual void update (const uint symbol, uint l) {
        cur_index++;
        symbol_rewards[symbol] =
                (1.0 * symbol_frequencies[symbol] * symbol_rewards[symbol]
                 + Code_value_bits - l)
                / (symbol_frequencies[symbol] + 1);
        symbol_frequencies[symbol]++;

        render_probabilities_from_rewards();
        recalculate_temperature();
    }

    virtual uint get_symbol () {
        std::discrete_distribution< uint > distribution(symbol_probabilities.begin(), symbol_probabilities.end());
        return distribution(generator);
    }

    virtual void reset () {
        cur_index = 0;
        T = initial_T;
        symbol_rewards = initial_symbol_rewards;
        symbol_probabilities.assign(symbol_count, 0.);
        symbol_frequencies.assign(symbol_count, 0);
        render_probabilities_from_rewards();
    }

    void render_probabilities_from_rewards () {
        auto max_symbol_reward = *std::max_element(symbol_rewards.begin(), symbol_rewards.end());
        for (size_t i = 0; i < symbol_count; i++) {
            symbol_probabilities[i] = std::exp((symbol_rewards[i] - max_symbol_reward) / T);
        }
    }

    void recalculate_temperature () {
        T = max(T * 0.995, 0.00001);
    }

    virtual void initialize (const uint _tact_count) {
        tact_count = _tact_count;
        for (auto &x : symbol_rewards) {
            x = rand() % max_col + 1;
        }
        initial_symbol_rewards = symbol_rewards;
        render_probabilities_from_rewards();
    }

    void print_symbol_probabilities () {
        std::cout << "Symbol probabilities" << std::endl;
        for (const auto cur_prob : symbol_probabilities) {
            std::cout << cur_prob << " ";
        }
        std::cout << std::endl;
    }

    void print_symbol_rewards () {
        std::cout << "Symbol rewards" << std::endl;
        for (const auto cur_rew : symbol_rewards) {
            std::cout << cur_rew << " ";
        }
        std::cout << std::endl;
    }
};

class ExpertSource : public SourceAgentBase
{
    std::vector< double > symbol_rewards;
    std::vector< double > initial_symbol_rewards;
    std::vector< uint > symbol_frequencies;
    std::default_random_engine generator;
    uint max_col;

    double T, initial_T;

public:

    virtual ~ExpertSource () {}

    ExpertSource (
            uint _max_col = 1,
            bool _check_change = false,
            double _temperature = 10000.) :
        SourceAgentBase(),
        symbol_rewards(symbol_count),
        initial_symbol_rewards(symbol_count),
        symbol_frequencies(symbol_count),
        generator(time(0)),
        max_col(_max_col),
        T(_temperature),
        initial_T(_temperature) {}

    virtual void update (const uint symbol, uint l) {
        ++cur_index;
        symbol_rewards[symbol] =
                (1.0 * symbol_frequencies[symbol] * symbol_rewards[symbol]
                 + Code_value_bits - l)
                / (symbol_frequencies[symbol] + 1);
        ++symbol_frequencies[symbol];
        recalculate_temperature();
    }

    virtual uint get_symbol () {
        int eps = rand() % 10000;
        if (eps < T) {
            return rand() % symbol_count;
        }
        auto mx = std::max_element(symbol_rewards.begin(), symbol_rewards.end());
        return std::distance(symbol_rewards.begin(), mx);
    }

    virtual void recalculate_temperature() {
        --T;
    }

    virtual void reset () {
        cur_index = 0;
        T = initial_T;
        symbol_rewards = initial_symbol_rewards;
        symbol_frequencies.assign(symbol_count, 0);
    }

    virtual void initialize (const uint _tact_count) {
        tact_count = _tact_count;
        for (auto &x : symbol_rewards) {
            x = rand() % max_col + 1;
        }
        initial_symbol_rewards = symbol_rewards;
    }

    void print_symbol_rewards () {
        std::cout << "Symbol rewards" << std::endl;
        for (const auto cur_rew : symbol_rewards) {
            std::cout << cur_rew << " ";
        }
        std::cout << std::endl;
    }
};

#endif
