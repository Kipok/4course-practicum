import numpy as np
from matplotlib import pyplot as plt
import decimal
from matplotlib.pyplot import savefig


def compute_error(x, y):
    er_dot_prod = decimal.Decimal(x.dot(y.T).sum())
    dot_prod = decimal.Decimal("0")
    for yi in y:
        for j in range(x.shape[0]):
            dot_prod += decimal.Decimal(x[j]) * decimal.Decimal(yi[j])
    abs_err = dot_prod - er_dot_prod
    rel_err = (dot_prod - er_dot_prod) / er_dot_prod
    return abs_err, rel_err


def compute_error_kahan(x, y):
    er_dot_prod = 0
    c = 0
    for yi in y:
        for j in range(x.shape[0]):
            c_prod = x[j] * yi[j]
            yy = c_prod - c
            t = er_dot_prod + yy
            c = (t - er_dot_prod) - yy
            er_dot_prod = t
    er_dot_prod = decimal.Decimal(er_dot_prod)
    dot_prod = decimal.Decimal("0")
    for yi in y:
        for j in range(x.shape[0]):
            dot_prod += decimal.Decimal(x[j]) * decimal.Decimal(yi[j])
    abs_err = dot_prod - er_dot_prod
    rel_err = (dot_prod - er_dot_prod) / er_dot_prod
    return abs_err, rel_err


if __name__ == '__main__':
    x = np.linspace(0, 500, 1000)
    y = 3 * 2 ** (-53) * 100 * 100 * x ** 2
    plt.plot(x, y)
    plt.ylim([0, 8.5*1e-7])
    plt.xlabel('max koordinate')
    plt.ylabel('max error')
    savefig('theory.png')

    decimal.getcontext().prec = 100

    sz = 1000
    kk = np.linspace(1, 100000, sz)
    kae = np.empty(sz)
    kre = np.empty(sz)
    ae = np.empty(sz)
    re = np.empty(sz)
    for i, k in enumerate(kk):
        x = np.random.rand(100) * k - k / 2
        y = np.random.rand(100, 100) * k - k / 2
        ae[i], re[i] = compute_error(x, y)
        kae[i], kre[i] = compute_error_kahan(x, y)

    plt.plot(kk, ae, label='Simple sum')
    plt.plot(kk, kae, label='Kahan sum')
    plt.xlabel('max koordinate')
    plt.ylabel('absolute error')
    plt.legend(loc=2)
    savefig('1.png')

    plt.plot(kk, re, label='Simple sum')
    plt.plot(kk, kre, label='Kahan sum')
    plt.xlabel('max koordinate')
    plt.ylabel('relative error')
    plt.legend(loc=2)
    savefig('2.png')

    x = np.random.rand(10) * 1000
    y_sm = np.random.rand(1000, 10) / 10000
    y_bg = np.random.rand(1000, 10) * 10000
    y = np.empty((2000, 10))
    y[0::2] = y_sm
    y[1::2] = y_bg
    ae, re = compute_error(x, y)
    kae, kre = compute_error_kahan(x, y)
    ae - kae
    sz = 1000
    kk = np.linspace(1, 100000, sz)
    kae = np.empty(sz)
    kre = np.empty(sz)
    ae = np.empty(sz)
    re = np.empty(sz)
    for i, k in enumerate(kk):
        x = np.random.rand(100) * k - k / 2
        y_sm = np.random.rand(50, 100) * k - k / 2
        y_bg = np.random.rand(50, 100) / k - 0.5 / k
        y = np.empty((100, 100))
        y[0::2] = y_sm
        y[1::2] = y_bg
        ae[i], re[i] = compute_error(x, y)
        kae[i], kre[i] = compute_error_kahan(x, y)
    plt.plot(kk, ae, label='Simple sum')
    plt.plot(kk, kae, label='Kahan sum')
    plt.xlabel('max koordinate')
    plt.ylabel('absolute error')
    plt.legend(loc=2)
    savefig('3.png')

    plt.plot(kk, re, label='Simple sum')
    plt.plot(kk, kre, label='Kahan sum')
    plt.xlabel('max koordinate')
    plt.ylabel('relative error')
    plt.legend(loc=2)
    savefig('4.png')
