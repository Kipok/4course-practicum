%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                  Класс документа для студенческих отчётов                 %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                 copyleft (GPL)       К.В.Воронцов,    2007                %%
%%                 copyleft (GPL)       Н.Ю.Золотых,     2010                %%
%%                 copyleft (GPL)       А.И.Майсурадзе,  2014                %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{prak417lua}[2014/10/20 Document class `prak417lua' for short reports]
%% Потребуем LuaTeX (хотя и pdfTeX со всем худо-бедно мог бы справиться)
\RequirePackage{ifluatex}
\ifluatex\else
 \ClassError{prak417lua}{LuaLaTeX is required}%
   {Please, compile using LuaLaTeX.\MessageBreak}
 \stop
\fi
%% Объявление опций пакета
% Пока опций нет
\DeclareOption*{%
 \ClassError{prak417lua}{Unknown option `\CurrentOption'}%
 %\PassOptionsToClass{\CurrentOption}{article}%
 %\OptionNotUsed%
}
%% Когда опции есть, здесь можно вызвать конфигурацию по умолчанию
%\ExecuteOptions{11pt,twoside,twocolumn}
%% Все опции к этому моменту должны быть объявлены
\ProcessOptions\relax% полезно \relax сразу после \ProcessOptions,
                     % чтобы звёздочку не искала система
%% Теперь загружаем стандартный класс, внешние опции в него не проникнут
\LoadClass[a4paper,10pt,twoside,notitlepage]{article}

%% Для печатающих команд разумно использовать \DeclareRobustCommand*

%% Этот размер используют многие пакеты, поэтому его лучше как можно раньше
%  и как можно проще задать
\topsep=0pt% это вертикальные отступы вокруг списка

%% В конец документа поместим дату трансляции
%% Почему-то \par туда не встаёт
\AtEndDocument{\hrule\raggedright Оттранслировано \today}

%% Типичные основные пакеты
\RequirePackage{ifthen} % a wide array of tests, \ifthenelse, \whiledo, FRAGILE, there is always a better option
%% If you want to use colour names, you need to load the package `xcolor'.
%% Use the package `xcolor' instead of `color'.
%% Options include several colour name groups.
%?option `table'
\RequirePackage[usenames,dvipsnames,svgnames,x11names]{xcolor} % driver-independent colour extensions for LaTeX
\RequirePackage{graphicx} % a key-value interface for optional arguments to the \includegraphics command

%% Традиционные пакеты с математическими шрифтами лучше вперёд ставить,
%% т.к. другие пакеты о них знают и подстраиваются под них.
%latexsym: U/lasy/m+b/n
%amsfonts: Computer Modern math (cmex, cmmib, cmbsy),
%amsfonts: Computer Modern small caps (cmcsc)
%amsfonts: Washington Cyrillic (wncyr, wncyb, wncyi, wncysc, wncyss)
%amsfonts: Washington Cyrillic vpl-files enable the use of these fonts with alternate encodings and transliteration schemes
%amsfonts: Euler Roman (eur), Euler Script (eus), Euler Fraktur (euf), Euler extra symbols (euex), AMS symbols A (msa), AMS symbols B (msb)
%amsfonts: \mathfrak=euf/m/n or euf/b/n, \mathbb=msb,
%amsfonts: U/msa/m/n, U/msb/m/n, U/euex/m/n, U/euf/m+b/n, U/eus/m+b/n, U/eur/m+b/n
\RequirePackage{amssymb} % all the symbols found in the AMS symbol fonts msam and msbm, includes `amsfonts', supersedes latexsym
\RequirePackage{mathrsfs} % Raph Smith's Formal Script font in mathematics, provides a \mathscr command
\RequirePackage{euscript} % Euler Script font in mathematics, provides a \EuScript command

%% Традиционный пакет amsmath лучше вперёд ставить,
%% т.к. другие пакеты о нём знают и подстраиваются под него.
\RequirePackage{amsmath} % enhancements for mathematical formulas

%! The package `inputenc' does not work with LuaLaTeX.
%! DON'T SAY \usepackage[cp1251]{inputenc}, it produces an error.
%! You may say \usepackage[utf8]{inputenc}, it is ignored.

%% Normally, LuaLaTeX runs in Unicode mode: reads UTF-8 encoded files and uses Unicode fonts.
%% Don't use the packages `inputenc' or `luainputenc'. Source file is to be converted to UTF-8 encoding (Unicode character set).
%% Don't use the package `fontenc'. Use Unicode fonts.
%% To invoke Unicode mode, one needs to load the package fontspec.
\RequirePackage{fontspec}
%\RequirePackage{unicode-math}
%% Default font features for the subsequent \setXXXfont commands can be declared with the \defaultfontfeatures.
%% Let's set the script and language for the following fonts. It has nothing to do with the package `babel'.
 \defaultfontfeatures{Renderer=Full,Script=Cyrillic,Language=Russian}
%% Default font features for the subsequent \setXXXfont commands can be added with the \defaultfontfeatures+.
%% Many fonts support a lot of different ligatures, we may turn on any group of them.
%\defaultfontfeatures+{Ligatures={Required,Common,Rare,Contextual,Historic,Logos,Rebus,Diphthong,Squared,AbbrevSquared,Icelandic}}
%% Traditional TeX ligatures may be emulated. They are emulated by default.
%\defaultfontfeatures+{Ligatures={TeX}}
%% Many contemporary fonts have either italic or slanted shape, but not both. Serif fonts have italic shape, sans-serif fonts have slanted shape.
%% If a font doesn't include slanted shape, we may emulate it, but we loose italic. By default, italic shape is used instead of slanted.
%\defaultfontfeatures+{AutoFakeSlant}
%% Unlike the traditional LaTeX, many contemporary fonts treat small caps as a shape variant, not distinct shape. It makes sence to query \itshape\scshape.
%%
%% Standard LaTeX classes use romanic, sans-serif and monospaced types.
%% Explicitly indicate which fonts should be used for romanic, sans-serif and monospaced types.
%% Computer Modern Unicode fonts are a part of all modern LaTeX distributions.
 \setmainfont{CMU Serif}
  % CMU Serif has no slanted shape, italic is substituted by default.
  % If you want different italic and slanted shapes,
  % you can add some options, something like [SlantedFont={Georgia},SlantedFeatures={FakeSlant},BoldSlantedFeatures={FakeSlant}]
%% For additional types to match the main font size, we use the option `Scale=MatchLowercase'.
%% For Computer Modern Unicode fonts it could be omitted: they match each other.
%% The option `Color' makes it easier to distinguish the types.
% \setsansfont{CMU Sans Serif}[Scale=MatchLowercase,Color=880088] % The font has no slanted shape, italic instead
% \setmonofont{CMU Typewriter Text}[Scale=MatchLowercase,Color=008888] % The font has no slanted shape, italic instead
% \setmathfont{Cambria Math}
%%
%% The \cyrdash defines a dash for Russian language in the package `babel-russian'.
%% As there is em-dash symbol in Unicode fonts, one can use it directly, it works well with copy-paste.
\chardef\cyrdash`\—%% Cyrillic dash glyph is Unicode em-dash now.
%% Without the \chardef, the \cyrdash may mean a group of symbols, it disturbs copy-paste.
%%
%% The language enlisted last in the list of options of the babel package
%% is assumed to be the main language of the document,
%% which is also active language right after \begin{document}.
%% If you are going to typeset long phrases or whole passages of English text in Russian document, add `babel-english'.
\RequirePackage[english,french,german,russian]{babel}
%%
%% From now on, \selectlanguage{russian} switches to Russian language, with the following effects:
%% 1. Russian hyphenation patterns are made active;
%% 2. \today prints the date in Russian;
%% 3. the LaTeX caption names are translated into Russian;
%% 4. em-dash typed by the ligature --- depends on the current encoding and engine;
%%    the ligature --- always produces em-dash in LuaTeX and XeTeX since these engines use the same encoding for all the languages;
%% 5. character " becomes active, it intriduces several punctuation commands;
%%    command "--- produces specific cyrillic dash, it differs from ---.
%%
%% Use \foreignlanguage{english}{...text...} to typeset small chunks of text in English.

%% Advanced language specific facilities for inline and display quotations
%% Include csquotes after the package `babel'
\RequirePackage[autostyle]{csquotes}

%% Работа с библиографическими ссылками
\RequirePackage[style=gost-numeric,backend=biber]{biblatex}

%% Работа с плавающими объектами
% пакет caption взаимодействует с пакетом floatrow для унификации плавающих объектов
\RequirePackage[hypcap=true,font=small,labelfont=bf,labelsep=period,justification=centerlast]{caption} % customising captions in floating environments
%\DeclareCaptionFont{white}{\color{white}}
%\DeclareCaptionFormat{listing}{\colorbox[cmyk]{0.43, 0.35, 0.35,0.01}{\parbox{\textwidth}{\hspace{15pt}#1#2#3}}}
%\captionsetup[lstlisting]{position=top,format=listing,labelfont=white,textfont=white, singlelinecheck=false, margin=0pt, font={bf,footnotesize}}
%\captionsetup[table]{position=top}
%\captionsetup[figure]{position=bottom}
%\captionsetup[algorithm]{position=top}
%% Пакет floatrow подменяет пакет float, поэтому его надо загрузить раньше всех пакетов, работающих с плавающими объектами
\RequirePackage{floatrow} % customising floating environments
\floatsetup[figure]{style=plain}
\floatsetup[table]{style=Plaintop}

%% Математические пакеты
\RequirePackage{array} % an extended implementation of the array and tabular environments
\RequirePackage[all]{xy} % a special package for drawing diagrams
\RequirePackage{pb-diagram} % the environment to create complex commutative diagrams

%% Гиперссылки
\RequirePackage[colorlinks,urlcolor=blue]{hyperref} % the ability to create hyperlinks within the document
\def\HyLang@russian{%
  \def\equationautorefname{выр.}%
  \def\footnoteautorefname{подстр.\ прим.}%
  \def\itemautorefname{п.}%
  \def\figureautorefname{рис.}%
  \def\tableautorefname{табл.}%
  \def\partautorefname{ч.}%
  \def\chapterautorefname{гл.}%
  \def\sectionautorefname{разд.}%
  \def\appendixautorefname{прил.}%
  \def\subsectionautorefname{подразд.}%
  \def\subsubsectionautorefname{подподразд.}%
  \def\paragraphautorefname{пар.}%
  \def\subparagraphautorefname{подпар.}%
  \def\FancyVerbLineautorefname{стр.}%
  \def\pageautorefname{с.}%
  \def\algorithmautorefname{алг.}%
  \def\listingautorefname{лист.}%
  \def\Theoremautorefname{теор.}%
  \def\Lemmaautorefname{лем.}%
  \def\Definitionautorefname{опр.}%
}
%\RequirePackage{breakurl} % multiline urls, include AFTER hyperref

%\RequirePackage{showframe}
% Печать на стандартной части листа A4
% INTERFACE
\newcommand\NormalAFOURMargins{
% левый срез массива текста=1in+\hoffset+\oddsidemargin
    \hoffset=0mm
    \oddsidemargin=0mm
    \evensidemargin=15mm
    \textwidth=145mm
% размеры маргиналий
    \marginparsep=3mm
    \marginparwidth=30mm
% верхний срез массива текста=1in+\voffset+\topmargin+\headheight+\headsep
    \voffset=-5mm
    \topmargin=0mm
    \headheight=20pt
    \headsep=5mm
    \textheight=246mm
    \footskip=20pt
}
% Печать во весь лист A4
% INTERFACE
\newcommand\MinAFOURMargins{
% левый срез массива текста=1in+\hoffset+\oddsidemargin
    \hoffset=-15mm
    \oddsidemargin=0mm
    \evensidemargin=15mm
    \textwidth=165mm
% размеры маргиналий
    \marginparsep=3mm
    \marginparwidth=30mm
% верхний срез массива текста=1in+\voffset+\topmargin+\headheight+\headsep
    \voffset=-15mm
    \topmargin=0mm
    \headheight=20pt
    \headsep=5mm
    \textheight=270mm
    \footskip=10pt
}
\NormalAFOURMargins
% Размер абзацного отступа, почему-то приходится позже устанавливать
\AtBeginDocument{\parindent=4.5ex}
% Расстояние между абзацами лучше сделать растяжимым
\AtBeginDocument{\parskip=0ex plus 2ex minus 0ex}

% \baselineskip, \lineskip and \lineskiplimit in TeX
% \baselinestretch, \linespread in LaTeX
% \footnotesep in LaTeX

%% Управление разбиением на строки внутри абзаца
\pretolerance=-1% качество этапа без переносов %-1=разбиение без переносов не выполняем
\tolerance=300% качество этапа с переносами
\hbadness=200% когда сообщать о плохих строках
% Выравниваем по левому срезу, переносы слов будут
%\raggedright\rightskip=0pt plus 1cm

% Управление нижним срезом страницы
%\flushbottom%все нижние срезы на одном уровне, нужно что-то по вертикали растянуть внутри страницы
\raggedbottom%нижние срезы не выравниваются
% Подавить эффект "висячих стpок" (маленькие кусочки абзаца остаются на другой странице)
% Это требует чего-то растягивающегося внутри страницы или переменного нижнего среза
\clubpenalty=10000
\widowpenalty=10000

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Оформление заголовков статей
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Настройки заголовка статьи
% INTERFACE
\newlength\vskipBeforeTitle
\setlength\vskipBeforeTitle{3ex}
\newlength\vskipAfterTitle
\setlength\vskipAfterTitle{0.5ex}
\newcommand\typeTitle[1]{\normalfont\Large\rmfamily\upshape\bfseries #1}
\newcommand\typeAuthor[1]{\normalfont\normalsize\rmfamily\itshape\bfseries #1}
\newcommand\typeEmail[1]{\normalfont\small\ttfamily #1}
\newcommand\typeOrganization[1]{\normalfont\small #1}
\newcommand\typeDate[1]{\normalfont\small #1}
%\newcommand\typeTocAuthorTitle[2]{{\unstretchspaces\itshape #1}\\ #2}

%! зачем??? между фамилией и инициалами авторов сразу надо правильные пробелы вставлять
% сделать неразрывные пробелы ещё и нерастяжимыми (например между фамилией и инициалами авторов)
% \newcommand\unstretchspaces{\catcode`~=\active\def~{\;}}

% Вывод заголовка
\renewcommand\maketitle{%
    \vskip\vskipBeforeTitle\noindent
    \begin{center}%
            {\typeTitle\@title}\\
            \vskip1ex%
            {\typeAuthor\@author}%
            \ifthenelse{\equal{\@email}{}}{}{\\\vskip0.5ex{\typeEmail\@email}}%
            \ifthenelse{\equal{\@organization}{}}{}{\\\vskip0.5ex{\typeOrganization\@organization}}%
            \ifthenelse{\equal{\@date}{}}{\\\vskip0.5ex{\typeDate\today}}{\\\vskip0.5ex{\typeDate\@date}}%
    \end{center}%
    \vskip\vskipAfterTitle%
    % сформировать колонтитулы
    \markboth{\@author@short}{\@title@short}%
    \par\nobreak\@afterheading
}

%% Очистка информации для титула
\newcommand\@clear@title{%
    \gdef\@author@short{}%
    \gdef\@author{}%
    \gdef\@title@short{}%
    \gdef\@title{}%
    \gdef\@organization{}%
    \gdef\@email{}%
    \gdef\@date{}%
}
\@clear@title
\renewcommand{\title}[2][]{\@clear@title
    \gdef\@title{#2}%
    \ifthenelse{\equal{#1}{}}%
        {\gdef\@title@short{{#2}}}%
        {\gdef\@title@short{{#1}}}%
}
\renewcommand{\author}[2][]{
    \gdef\@author{#2}
    \ifthenelse{\equal{#1}{}}%
        {\gdef\@author@short{{#2}}}%
        {\gdef\@author@short{{#1}}}%
}
\newcommand{\organization}[1]{\gdef\@organization{{#1}}}
\newcommand{\email}[1]{\gdef\@email{{#1}}}
\renewcommand{\date}[1]{\gdef\@date{{#1}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Переопределение колонтитулов
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\renewcommand{\ps@headings}{%
    \renewcommand{\@oddfoot}{}%
    \renewcommand{\@oddhead}{\parbox{\textwidth}{\footnotesize
        \rightmark\hfill{Практикум на ЭВМ}\quad\thepage\\[-2ex]\hrule}}%
    \renewcommand{\@evenfoot}{}%
    \renewcommand{\@evenhead}{\parbox{\textwidth}{\footnotesize
        \thepage\quad{Практикум на ЭВМ}\hfill\leftmark\\[-2ex]\hrule}}%
}
\renewcommand{\ps@empty}{%
    \renewcommand{\@oddfoot}{}%
    \renewcommand{\@oddhead}{\parbox{\textwidth}{\centering\scshape\footnotesize
        Московский государственный университет имени М. В. Ломоносова\\
        Факультет вычислительной математики и кибернетики\\
        Кафедра математических методов прогнозирования}}%
    \renewcommand{\@evenfoot}{}%
    \renewcommand{\@evenhead}{\parbox{\textwidth}{\centering\scshape\footnotesize
        Московский государственный университет имени М. В. Ломоносова\\
        Факультет вычислительной математики и кибернетики\\
        Кафедра математических методов прогнозирования}}%
}
% все страницы имеют такое оформление
\pagestyle{headings}
% первая страница имеет особое оформление
\thispagestyle{empty}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Оформление разделов
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Разделы
% INTERFACE
\let\part\@undefined
\let\chapter\@undefined
% Отображения номера в заглавии раздела
\renewcommand*{\@seccntformat}[1]{\csname the#1\endcsname. \relax}
\renewcommand\section{\@startsection{section}{1}{\z@}%
                                   {3.5ex \@plus1ex \@minus.2ex}%
                                   {2.3ex \@plus.2ex}%
                                   {\normalfont\Large\bfseries\scshape}}
\let\subsection\@undefined
\let\subsubsection\@undefined
\renewcommand\paragraph{\@startsection{paragraph}{4}{\parindent}%
                                    {3.25ex \@plus1ex \@minus.2ex}%
                                    {-1em}%
                                    {\normalfont\normalsize\bfseries\scshape}}
\let\subparagraph\@undefined

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Оформление оглавления
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\renewcommand\contentsname{Содержание}

% переопределение команды генерации оглавления
%\renewcommand\tableofcontents{%
%    \if@twocolumn\@restonecoltrue\onecolumn\else\@restonecolfalse\fi
%    \par\noindent{\normalfont\Large\rmfamily\bfseries\contentsname}\nopagebreak\par\bigskip
%    \markboth{\contentsname}{\contentsname}%
%    \@starttoc{toc}%
%    \if@restonecol\twocolumn\fi
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Оформление библиографии, в каждой статье отдельно
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Оформление элементов пункта библиографии
% INTERFACE
%\def\BibAuthor#1{\emph{#1}}
%\def\BibTitle#1{#1}
%\def\BibUrl#1{{\small\url{#1}}}
%\def\BibHttp#1{{\small\url{http://#1}}}
%\def\BibFtp#1{{\small\url{ftp://#1}}}
%\def\typeBibItem{\small\sloppy}

% Переопределение горизонтальных и вертикальных промежутков в списке литературы
%\renewenvironment{thebibliography}[1]
%    {\section{\bibname}%
%        \list{\@biblabel{\@arabic\c@enumiv}}{%
%            \settowidth\labelwidth{\@biblabel{#1}}%
%            \leftmargin\labelwidth
%            \advance\leftmargin by 1ex%
%            \topsep=0pt\parsep=3pt\itemsep=0ex%
%            \@openbib@code
%            \usecounter{enumiv}%
%            \let\p@enumiv\@empty
%            \renewcommand\theenumiv{\@arabic\c@enumiv}%
%        }%
%        \typeBibItem
%%        \clubpenalty4000%
%%        \@clubpenalty\clubpenalty
%%        \widowpenalty4000%
%%        \sfcode`\.\@m%
%    }{%
%        \def\@noitemerr{\@latex@warning{Empty `thebibliography' environment}}%
%        \endlist
%    }

% Действия, которые делаются в начале каждой статьи
% (как в сборнике, так и при отдельной компиляции)
%\newcommand{\@BeginDocument}{
%    \pagestyle{headings}%
%}
\AtBeginDocument{%
    % Переопределение списков с меньшими интервалами (слегка экономим бумагу)
    \renewcommand{\@listi}{%
        \topsep=0pt plus 1pt% дополнительно к \parskip вокруг списка
        \partopsep=0pt plus 1pt% дополнительно к \topsep вокруг списка, если начат новый абзац
        \parsep=0pt% между абзацами внутри пункта
        \itemsep=0pt plus 1pt% дополнительно к \parsep между пунктами
        \itemindent=0pt% абзацный выступ
        \listparindent=\parindent% абзацный отступ
        \labelsep=\parindent% расстояние до метки
        \labelwidth=0pt% ширина метки
        \leftmargin=0pt% отступ слева
        \rightmargin=0pt% отступ справа
    }%
    \renewcommand{\@listii}{\@listi\topsep=0pt}%
    \renewcommand{\@listiii}{\@listii}%
    \renewcommand{\@listiv}{\@listiii}%
    \renewcommand{\labelitemi}{^^^^2022}% Unicode bullet
    \renewcommand{\labelitemii}{^^^^2022^^^^2022}%
    \renewcommand{\labelitemiii}{^^^^2022^^^^2022^^^^2022}%
    \renewcommand{\labelitemiv}{^^^^2022^^^^2022^^^^2022^^^^2022}%
    \renewcommand{\theenumii}{\asbuk{enumii}}%
}
\hyphenation{ммро рффи}%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Поддержка рецензирования и корректуры
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\RequirePackage[colorinlistoftodos,leadertype=sBezier,%
%positioning=sLeaderNorthEastBelowStacks]{luatodonotes}
%\@todonotes@SetTodoListName{Список задач}%
%\@todonotes@SetMissingFigureText{Иллюстрация}%
%\@todonotes@SetMissingFigureUp{Иллюстрация}%
%\@todonotes@SetMissingFigureDown{отсутствует}%
\newcounter{praknotes}
\def\thepraknotes{\arabic{praknotes}}
% \todo ends with \ignorespaces, so there is no need to include it in the following defs
% INTERFACE
\newcommand\TODO[1]{%
    \refstepcounter{praknotes}%
    \todo[color=yellow,size=\small,caption={\thepraknotes}]{\thepraknotes. #1}}
\newcommand\FIXIT[1]{%
    \refstepcounter{praknotes}%
    \todo[color=red!40,size=\small,caption={\thepraknotes}]{\thepraknotes. #1}}
\newcommand\REVIEWERNOTE[2][1]{%
    \refstepcounter{praknotes}%
    \todo[color=green!40,size=\small,caption={\thepraknotes},
          author={\thepraknotes. #1}]{#2}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Для включения графиков пакетом graphicx
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\DeclareGraphicsRule{.wmf}{bmp}{}{}
\DeclareGraphicsRule{.emf}{bmp}{}{}
\DeclareGraphicsRule{.bmp}{bmp}{}{}
\DeclareGraphicsRule{.png}{bmp}{}{}
% Для подписей на рисунках, вставляемых includegraphics
\def\XYtext(#1,#2)#3{\rlap{\kern#1\lower-#2\hbox{#3}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Плавающие иллюстрации
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\setcounter{topnumber}{9}
%\setcounter{totalnumber}{9}
%\renewcommand\topfraction{1.0}
%\renewcommand\textfraction{0.0}
%\renewcommand\floatpagefraction{0.01} % float-страниц быть вообще не должно - это чтобы их лучше видеть ;)
%\setlength\floatsep{2ex}
%\setlength\textfloatsep{2.5ex}
%\setlength\intextsep{2.5ex}
%\setlength\abovecaptionskip{2ex}

%\def\@caption@left@right@skip{\leftskip=3.5ex\rightskip=3.5ex}
%\def\nocaptionskips{\def\@caption@left@right@skip{}}

%\renewcommand\@makecaption[2]{%
%    \vskip\abovecaptionskip
%    \sbox\@tempboxa{\small\textbf{#1.} #2}%
%    \ifdim\wd\@tempboxa >\hsize
%        {\@caption@left@right@skip\small\textbf{#1.} #2\par}
%    \else
%        \global\@minipagefalse
%        \hb@xt@\hsize{\hfil\box\@tempboxa\hfil}%
%    \fi
%    %\vskip\belowcaptionskip
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Некоторые переопределения для унификации математики
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Унифицируем математические символы
\let\ge\geqslant
\let\geq\geqslant
%\let\geqslant\@undefined
\let\le\leqslant
\let\leq\leqslant
%\let\leqslant\@undefined
\let\emptyset\varnothing
\let\kappa\varkappa
\let\phi\varphi
\let\epsilon\varepsilon

% Перенос знака операции на следующую строку % Всё равно полностью не автоматизирует
\newcommand\brop[1]{#1\discretionary{}{\hbox{$#1$}}{}}

%\renewcommand{\vec}[1]{\mathbf{#1}} % \vec is an accent, no interpretation
%\renewcommand{\complement}{\mathsf{C}} % \complement is a symbol from amsfonts
\newcommand\vect[1]{\vec{#1}}
\newcommand\mtrx[1]{\mathscr{#1}}
\providecommand{\T}{}
\renewcommand{\T}{^\intercal}

% уточнение внешнего вида операторов
\newcommand\myop[1]{\mathop{\operator@font #1}\nolimits}
\newcommand\mylim[1]{\mathop{\operator@font #1}\limits}

\renewcommand\lim{\mylim{lim}}
\renewcommand\limsup{\mylim{lim\,sup}}
\renewcommand\liminf{\mylim{lim\,inf}}
\renewcommand\max{\mylim{max}}
\renewcommand\min{\mylim{min}}
\renewcommand\sup{\mylim{sup}}
\renewcommand\inf{\mylim{inf}}
\newcommand\argmin{\mylim{arg\,min}}
\newcommand\argmax{\mylim{arg\,max}}
\newcommand\Tr{\myop{tr}}
\newcommand\rank{\myop{rank}}
\newcommand\diag{\myop{diag}}
\newcommand\sign{\mylim{sign}}
\newcommand\const{\myop{const}}

% теория вероятностей
\newcommand{\erf}{\myop{erf}}
\newcommand{\Expect}{\mathsf{E}}
\newcommand{\Var}{\mathsf{V}}
\newcommand\Normal{\mathcal{N}}
\newcommand\cond{\mid}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Перечни
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Нумерованный перечень со скобками
\newenvironment{enumerate*}
{\renewcommand{\@listi}{%
        \topsep=\smallskipamount % вокруг списка
        \parsep=0pt% между абзацами внутри пункта
        \parskip=0pt% между абзацами
        \itemsep=0pt% между пунктами
        \itemindent=0ex% абзацный выступ
        \labelsep=1.5ex% расстояние до метки
        \leftmargin=7ex% отступ слева
        \rightmargin=0pt} % отступ справа
    \begin{enumerate}%
    \renewcommand\labelenumi{\theenumi)}%
}{%
    \end{enumerate}%
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Новые теоремоподобные окружения
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\RequirePackage{theorem} % obsolete, consider using ntheorem instead % enhancements to LATEX’s theorem environments
\RequirePackage[thmmarks,amsmath,hyperref]{ntheorem} % enhancements to LATEX’s theorem environments %no thref, no framed

\theoremstyle{plain} % Used for theorems, lemmas, propositions, etc.
\theoremseparator{.}
%\theoremindent 3.5ex
\theorempreskip{0pt plus 1ex}
\theorempostskip{0pt plus 1ex}
%\theoremsymbol{}
%\theoremframecommand{\colorbox[rgb]{1,.9,.9}}
%\newshadedtheorem{theorem}{Теорема}
\newtheorem{Theorem}{Теорема}
\newtheorem{Lemma}[Theorem]{Лемма}
\newtheorem{Proposition}[Theorem]{Утверждение}
%\newtheorem{state}[theorem]{Утверждение}
\newtheorem{Corollary}[Theorem]{Следствие}

\theoremstyle{margin} %	Used for definitions and examples
\theoremseparator{.}
%\theoremindent 3.5ex
\theorempreskip{0pt plus 1ex}
\theorempostskip{0pt plus 1ex}
\newtheorem{Definition}[Theorem]{Определение}
\newtheorem{Example}[Theorem]{Пример}
\newtheorem{Problem}[Theorem]{Задача}

\theoremstyle{break} %	Used for remarks and notes
\theoremseparator{.}
%\theoremindent 3.5ex
\theorempreskip{0pt plus 1ex}
\theorempostskip{0pt plus 1ex}
\newtheorem{Remark}[Theorem]{Примечание}
\newtheorem{Hint}[Theorem]{Указание}
\newtheorem{Hypothesis}[Theorem]{Гипотеза}
\newtheorem{Axiom}[Theorem]{Аксиома}

\theoremstyle{nonumberplain} %	Used for proofs
\theoremseparator{.}
\theoremindent 3.5ex
\theorempreskip{0pt}
\theorempostskip{0pt plus 1ex}
\newtheorem{Proof}{Доказательство}

%\newcommand{\qedsymb}{\rule{0.2em}{0.5em}}
%\newenvironment{proof}{\noindent{\itshape Доказательство. }}{\ \hfill\qedsymb\par\smallskip}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Оформление алгоритмов в пакете algorithmic
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage{algorithmic} % typesetting pseudocode, a part of the `algorithms' package
% удобнее самому, чем \RequirePackage{algorithm} % a floating environment designed to work with the algorithmic style, a part of the `algorithms' package
\DeclareNewFloatType{algorithm}{placement=htbp,name=Алгоритм}
\floatsetup[algorithm]{style=Plaintop}
\newcommand{\listalgorithmname}{Список алгоритмов}
\newcommand{\listofalgorithms}{\listof{algorithm}{\listalgorithmname}}

% переопределения (русификация) управляющих конструкций:
\renewcommand{\algorithmiccomment}[1]{\{ \textit{#1} \}}
\newcommand{\algKeyword}[1]{\textbf{#1}}
\renewcommand{\algorithmicrequire}{\algKeyword{Предусловие:}}
\renewcommand{\algorithmicensure}{\algKeyword{Постусловие:}}
\renewcommand{\algorithmicend}{\algKeyword{конец}}
\renewcommand{\algorithmicif}{\algKeyword{если}}
\renewcommand{\algorithmicthen}{\algKeyword{то}}
\renewcommand{\algorithmicelse}{\algKeyword{иначе}}
\renewcommand{\algorithmicelsif}{\algorithmicelse\ \algorithmicif}
\renewcommand{\algorithmicendif}{\algorithmicend\ \algorithmicif}
\renewcommand{\algorithmicfor}{\algKeyword{для}}
\renewcommand{\algorithmicforall}{\algKeyword{для всех}}
\renewcommand{\algorithmicdo}{\algKeyword{выполнить}}
\renewcommand{\algorithmicendfor}{\algorithmicend\ \algorithmicfor}
\renewcommand{\algorithmicwhile}{\algKeyword{пока}}
\renewcommand{\algorithmicendwhile}{\algorithmicend\ \algorithmicwhile}
\renewcommand{\algorithmicloop}{\algKeyword{цикл}}
\renewcommand{\algorithmicendloop}{\algorithmicend\ \algorithmicloop}
\renewcommand{\algorithmicrepeat}{\algKeyword{повторять}}
\renewcommand{\algorithmicuntil}{\algKeyword{пока-не}}
\renewcommand{\algorithmicprint}{\algKeyword{печатать}}
\renewcommand{\algorithmicreturn}{\algKeyword{возврат}}
%\newcommand{\algorithmicand}{\textbf{and}}
%\newcommand{\algorithmicor}{\textbf{or}}
%\newcommand{\algorithmicxor}{\textbf{xor}}
%\newcommand{\algorithmicnot}{\textbf{not}}
%\newcommand{\algorithmicto}{\textbf{to}}
\renewcommand{\algorithmicinputs}{\algKeyword{Вход:}}
\renewcommand{\algorithmicoutputs}{\algKeyword{Выход:}}
\renewcommand{\algorithmicglobals}{\algKeyword{Внешние переменные:}}
\renewcommand{\algorithmicbody}{}
%\newcommand{\algorithmictrue}{\textbf{true}}
%\newcommand{\algorithmicfalse}{\textbf{false}}

% Дополнительные команды для описания алгоритмов
\newcommand{\Procedure}[1]{\texttt{#1}}
\newcommand{\PROCEDURE}[1]{\medskip\STATE\algKeyword{ПРОЦЕДУРА} \Procedure{#1}}
% \INPUTS??? \newcommand\PARAMS{\renewcommand{\algorithmicrequire}{\algKeyword{Параметры:}}\REQUIRE}
% \BODY??? \newcommand\BEGIN{\\[1ex]\hrule\vskip 1ex}
% \ENDBODY??? \newcommand\END{\vskip 1ex\hrule\vskip 1ex}
% такой есть??? \newcommand\RETURN{\algKeyword{вернуть} }
% это не возврат??? \newcommand\EXIT{\algKeyword{выход}}
\newcommand{\onelineIF}[2]{\STATE\algorithmicif\ {#1}\ \algorithmicthen\ {#2}}

%! в новых версиях algorithmic это не требуется
% чтобы можно было ссылаться на шаги алгоритма
%\newenvironment{Algorithm}[1][t]%
%    {\begin{algorithm}[#1]\begin{algorithmic}[1]%
%        \renewcommand{\ALC@it}{%
%            \refstepcounter{ALC@line}% удивительно, почему это не сделал Peter Williams?
%            \addtocounter{ALC@rem}{1}%
%            \ifthenelse{\equal{\arabic{ALC@rem}}{1}}{\setcounter{ALC@rem}{0}}{}%
%            \item}}%
%    {\end{algorithmic}\end{algorithm}}

% чтобы поставить точечку после номера алгоритма в \caption:
%\renewcommand\floatc@ruled[2]{\vskip2pt\textbf{#1.} #2\par}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Оформление листингов в пакете listings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\RequirePackage{listings} % an environment to include the source code of any programming language, can be floating
\lstset{
         basicstyle=\ttfamily\footnotesize\addfontfeature{Color=black},   % Standardschrift
         breaklines=true,            % Zeilen werden Umgebrochen
         showspaces=false,           % Leerzeichen anzeigen ?
         tabsize=4,                  % Groesse von Tabs
         showtabs=false,             % Tabs anzeigen ?
         keywordstyle=\bfseries\addfontfeature{Color=black},
         identifierstyle=\addfontfeature{Color=brown},
         commentstyle=\itshape\addfontfeature{Color=gray},
         stringstyle=\itshape\addfontfeature{Color=green},  % Farbe der String
         showstringspaces=true,      % Leerzeichen in Strings anzeigen ?
         numbers=left,               % Ort der Zeilennummern
         numberstyle=\tiny,          % Stil der Zeilennummern
%         stepnumber=2,               % Abstand zwischen den Zeilennummern
         numbersep=5pt,              % Abstand der Nummern zum Text
         extendedchars=true,         %
 %        frame=b,
 %        frame=TrBl,
 %        frameround=ftft,
 %        keywordstyle=[1]\textbf,    % Stil der Keywords
 %        keywordstyle=[2]\textbf,    %
 %        keywordstyle=[3]\textbf,    %
 %        keywordstyle=[4]\textbf,   \sqrt{\sqrt{}} %
         xleftmargin=17pt,
         framexleftmargin=17pt,
         framexrightmargin=5pt,
         framexbottommargin=4pt,
 %        backgroundcolor=\color{lightgray},
}
\lstloadlanguages{% Check Documentation for further languages ...
%         %Pascal,
         C,
         C++,
%         Fortran,
%         Java,
%         %XML,
%         %HTML,
         Python,
%         Lua, error!
         Matlab,
%         R,
%         [LaTeX]TeX,
%         Gnuplot
}
\DeclareNewFloatType{listing}{placement=htbp,name=Листинг}
\floatsetup[listing]{style=Plaintop}
\newcommand{\listlistingname}{Список листингов}
\newcommand{\listoflistings}{\listof{listing}{\listlistingname}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Рисование нейронных сетей и диаграмм
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newenvironment{network}%
    {\catcode`"=12\begin{xy}<1ex,0ex>:}%
    {\end{xy}\catcode`"=13}
\def\nnNode"#1"(#2)#3{\POS(#2)*#3="#1"}
\def\nnLink"#1,#2"#3{\POS"#1"\ar #3 "#2"}
\def\nnSig{%
    \underline{{}^\sigma\:\mathstrut}\vrule%
    \overline{\phantom{()}}}
\def\nnTheta{%
    \underline{{}^\theta\:\mathstrut}\vrule%
    \overline{\phantom{()}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   П Р О Ч Е Е
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Для таблиц: невидимая вертикальная линейка немного расширяет заголовок
\newcommand{\headline}{\hline\rule{0pt}{2.5ex}}

% Остановим шаловливые ручки
\renewcommand*{\usepackage}[2][]{}
