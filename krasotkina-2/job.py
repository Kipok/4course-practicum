from mrjob.job import MRJob

class CheckNonNegative(MRJob):
    def mapper(self, _, line):
        yield "has_nonzero", reduce(lambda a, b: a == True or b == True,
                                        map(lambda x: x != '0', list(line)))
    def reducer(self, key, values):
        yield key, True in values

CheckNonNegative.run()
