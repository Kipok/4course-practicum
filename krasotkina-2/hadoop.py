from mrjob.job import MRJob
import numpy as np
import sys, os

class PracticeMatrix:
    def __init__(self, has_zero='false', n=1000, m=1000, local='local'):
        self.outfile = 'out.txt'
        self.has_zero = has_zero == 'true'
        self.n = int(n)
        self.m = int(m)
        self.local = local == 'local'
        self.gen_file()

    def gen_file(self):
        with open(self.outfile, 'w') as f:
            for i in xrange(self.n):
                for j in xrange(self.m):
                    if i != self.n - 1 or j != self.m - 1:
                        f.write('0');
                if i != self.n - 1:
                    f.write('\n')
            if self.has_zero:
                f.write('1\n')
            else:
                f.write('0\n')


    def parallel_compute(self):
        if self.local:
            os.system('python job.py ' + self.outfile)
        else:
            os.system('python job.py -r emr ' + self.outfile + ' --conf-path mrjob.conf')

    def sequential_compute(self):
        with open(self.outfile, 'r') as f:
            for line in f:
                if reduce(lambda a, b: a == True or b == True,
                          map(lambda x: x != '0', list(line[:-1]))):
                    print 'has_nonzero True'
                    return
            print 'has_nonzero False'


if __name__ == '__main__':
    pm = PracticeMatrix(*sys.argv[1:-1])
    if sys.argv[-1] == 'p':
        pm.parallel_compute()
    else:
        pm.sequential_compute()
